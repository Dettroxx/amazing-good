<?php

use Illuminate\Http\Request;

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::namespace('Api')->group(function () {
    Route::get('/users', 'UsersController@index');
});

//Для генерации новых пользователей
Route::get('gen-users', 'AdminToolsController@generateUsers');

//Для утилитных элементов
Route::group(['prefix' => 'utils',  'middleware' => 'auth:api'], function () {
    Route::prefix('navigator')->group(function () {
        Route::get('status', 'UtilsFetchController@status');
        Route::get('statuses', 'PostPreviewController@statuses');
        Route::get('start-range', 'UtilsFetchController@startRange');
        Route::post('delete-orphans', 'UtilsFetchController@deleteOrphans');
        Route::post('delete-post/{id}', 'UtilsFetchController@deletePost');
        Route::post('restore-post/{id}', 'UtilsFetchController@restorePost');
        Route::post('save-dots/{id}', 'UtilsFetchController@saveDots');
        Route::post('delete-dot/{id}', 'UtilsFetchController@deleteDot');
        Route::post('save-dot/{id}', 'UtilsFetchController@saveDot');
        Route::post('delete-offerlink/{id}', 'UtilsFetchController@deleteOfferlink');
        Route::get('get-offers-tree-node/{id?}', 'UtilsFetchController@getOfferCategoryTreeNode');
        Route::get('get-offers-tree', 'UtilsFetchController@getOfferCategoryTree');
        Route::post('add-new-offerlink', 'UtilsFetchController@addNewOfferLink');
        Route::post('add-new-offerlink-list', 'UtilsFetchController@addNewOfferLinkList'); 
        Route::get('get-logs-statuses', 'UtilsFetchController@getLogsStatuses');
        Route::get('get-logs/{id}', 'UtilsFetchController@getLogs');
        Route::get('get-invalid-offers', 'UtilsFetchController@getInvalidOffers');
        Route::post('deactivate-offer/{id}', 'UtilsFetchController@deactivateOffer');
        Route::post('replace-offer/{before}/{after}', 'UtilsFetchController@replaceOffer');

        //Списки для группировки элементов
        Route::get('get-lists-group/{id}/{offer_id}', 'UtilsFetchController@getGroupsList');
        Route::post('add-lists-group/{name}/{category_id}', 'UtilsFetchController@addGroupsList'); 
        Route::post('unlink-lists-group/{id}', 'UtilsFetchController@unlinkGroupsList'); 
        Route::post('link-lists-group/{id}/{list_id}', 'UtilsFetchController@linkGroupsList'); 
        Route::get('get-toplist/{id}', 'UtilsFetchController@getTopList');
    });
});


//Для статистики
Route::group(['prefix' => 'stat',  'middleware' => 'auth:api'], function () {
    Route::get('status', 'StatController@status');
    Route::post('handle', 'StatController@handle');
});

Route::get('checkwork', 'UtilsFetchController@checkwork');

//Для тестов
Route::prefix('test')->group(function () {
    Route::prefix('amazon')->group(function () {
        Route::get('update-offers', 'AmazonParse@getUpdateOffers');
        Route::get('parse-offers', 'AmazonParse@parseOffers');
        Route::get('amazon-import','AmazonParse@parseFile');
        Route::get('parse', 'AmazonParse@parse');
        Route::get('recursive', 'AmazonParse@recursiveTest');
        Route::get('excludeids', 'OfferListController@getIdsExclude');
    });

    Route::prefix('instagram')->group(function () {
        Route::get('parse-posts', 'InstagramParseController@parsePosts');
        Route::get('parse', 'InstagramParseController@instagramParse');
    });
});

//Для страницы навигатора
Route::prefix('navigator')->group(function () {
    Route::get('postpreview', 'PostPreviewController@index');
    Route::get('postpreview_test', 'PostPreviewController@test');
    Route::post('offerslist', 'OfferListController@index');
    Route::get('post/{id}', 'PostPreviewController@post');
    Route::get('post-offers/{id}', 'OffersController@index');
    Route::post('post-save-data/{id}', 'PostController@saveData');
    Route::get('post-offers-dot/{tag_id}', 'OffersController@offersDot');
});

//Для страницы конфигурации
Route::group(['prefix' => 'config',  'middleware' => 'auth:api'], function() {
    //Инстаграм конфиг
    Route::prefix('instagram-config')->group(function() {
        //Для тегов
        Route::get('load-tags/{type}', 'Config\InstagramConfig@loadTags');
        Route::post('add-tag', 'Config\InstagramConfig@addTag');
        Route::post('delete-tag/{id}', 'Config\InstagramConfig@deleteTag');
        Route::post('change-tag/{id}', 'Config\InstagramConfig@changeTag');
    });

    //Настройка для категорий тегов
    Route::prefix('tags-config')->group(function() {
        //Общие элементы
        Route::get('load-categories', 'Config\TagsAndCategoriesController@loadCetegories');
        Route::get('tags/{id}', 'Config\TagsAndCategoriesController@tags');

        //Для категорий
        Route::post('category-add', 'Config\TagsAndCategoriesController@categoryAdd');
        Route::post('category-delete/{id}', 'Config\TagsAndCategoriesController@categoryDelete');
        Route::post('category-change-order/{id}', 'Config\TagsAndCategoriesController@categoryChangeOrder');
        Route::post('category-save/{id}', 'Config\TagsAndCategoriesController@categorySave');

        //Для тегов
        Route::post('tag-add', 'Config\TagsAndCategoriesController@tagAdd');
        Route::post('tag-delete/{id}', 'Config\TagsAndCategoriesController@tagDelete');
        Route::post('tag-save/{id}', 'Config\TagsAndCategoriesController@tagSave');
    });


    //Конфигурация прокси и UserAgent
    Route::prefix('proxy-config')->group(function() {
        Route::get('load', 'Config\ProxyController@load');
        Route::post('save', 'Config\ProxyController@save');   
    });


    //Конфигурация Amazon
    Route::prefix('amazon-config')->group(function() {
        Route::get('load', 'Config\AmazonCategoryConfigController@load');
        Route::post('save', 'Config\AmazonCategoryConfigController@save');   
    });
});

//Список данных, необходимых для обновления
Route::get('file/update-categories', 'Config\AmazonCategoryConfigController@getUpdateCategories'); 
Route::get('file/update-new-offers', 'ImportAmazonCategoryLink@getUpdateOffers');
Route::get('file/update-offers', 'AmazonParse@getUpdateOffers');
Route::get('file/update-proxies', 'Config\ProxyController@getUpdateProxies');
Route::get('file/update-agents', 'Config\ProxyController@getUpdateAgents');
Route::get('file/update-tags', 'InstagramParseController@getUpdateTags'); 


//Для экспорта
Route::prefix('wp-export')->group(function() {
    Route::get('products', 'WPExport@products'); 
    Route::get('filters', 'WPExport@filters'); 
    Route::get('items', 'WPExport@items'); 
    Route::get('collections', 'WPExport@collections'); 
    Route::get('test', 'WPExport@test'); 
});

//TEST_SMALL_IMAGES
Route::get('testy', 'PostPreviewController@test');

Route::get('changeuserpass', 'HomeController@changeUserPassword'); 