<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TagCategory extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id', 'name', 'order'
    ];
}
