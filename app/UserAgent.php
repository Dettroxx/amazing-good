<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAgent extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'useragent'
    ];
}
