<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramComment extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id', 'post_id', 'user_id', 'text', 'created'
    ];
}
