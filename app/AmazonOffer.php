<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmazonOffer extends Model
{
    protected $casts = [
        'path' => 'array',
        'images' => 'array',
        'thumbs' => 'array',
        'sizes' => 'array',
        'colors' => 'array',
        'content' => 'array',
        'stars' => 'double'
    ];

    public $timestamps = false;
    protected $fillable = ['offer_id', 'path', 'title', 'stars', 'images', 'thumbs',
        'price_current', 'price_old', 'price_discount', 'status', 'status_code',
        'sizes', 'colors', 'content', 'parsed', 'url', 'created', 'updated', 'valid'];
}
