<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramAccount extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'username', 'password'
    ];
}
