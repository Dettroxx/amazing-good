<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmazonCategory extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id', 'name', 'url', 'sort'
    ];
}
