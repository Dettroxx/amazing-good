<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryOfferList extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id', 'list_id', 'offer_id'
    ]; 
}
