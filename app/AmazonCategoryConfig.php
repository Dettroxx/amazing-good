<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmazonCategoryConfig extends Model
{
    public $timestamps = false;
    protected $fillable = ["category"];
}
