<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\AmazonParse;
use App\Libraries\Amazon\AmazonParser;

class AParse extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cron:aoffers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Производит импорт офферов из временной таблицы';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new AmazonParse();
        $controller->parseOffers();
    }
}
