<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Http\Controllers\WPExport;

class CheckImages extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'instagram:check_images';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Проверяет изображения инстаграм';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $controller = new WPExport();
        $controller->Instamage();
    }
}
