<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramUser extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id', 'user_name', 'user_pic',
    ];
}
