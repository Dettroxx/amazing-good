<?php

namespace App\Import;

use Illuminate\Database\Eloquent\Model;

class ImportInstagramPreview extends Model
{
    public $timestamps = false;
    protected $fillable = ['code', 'url', 'images', 'caption', 'tags', 'tag_first'];
}
