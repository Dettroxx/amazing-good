<?php

namespace App\Import;

use Illuminate\Database\Eloquent\Model;

class ImportAmazonPreview extends Model
{
    public $timestamps = false;
    protected $fillable = ['offer_id', 'path', 'title', 'stars', 'images', 'thumbs', 'status', 'sizes', 'colors', 'content', 'price', 'old_price', 'url'];
}
