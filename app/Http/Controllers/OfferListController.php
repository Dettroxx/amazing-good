<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\OfferListResource;
use App\AmazonOffer;
use App\InstagramDotLink;
use DB;

class OfferListController extends Controller
{
    public function index(Request $request) {
        $filter = $request->get("filter");
        if (empty($filter)) return;

        $offers = $this->getOffers($filter);
        $data = OfferListResource::collection($offers);
        return $data;
    }

    private function getOffers($filter) {
        $per_page = $filter["meta"]["per_page"];
        $dotId = $filter["dotId"];
        $excludes = $this->getIdsExclude($dotId);
        $query = AmazonOffer::select("*", "amazon_offers.id as offer_target_id")->whereNotIn('amazon_offers.id', $excludes)->orderBy("valid", "desc")->orderBy("stars", "desc");
        $query = $this->getCategoryExclude($filter, $query);
        $query = $this->getSearchQuery($filter, $query);
        $offers = $query->paginate($per_page);
        return $offers;
    }

    //Возвращает список уже прикрепленных к текущей категории ресурсов, чтобы не выводить их в общем списке
    private function getIdsExclude($dotId) {
        $links = DB::table("post_offer_links")
                    ->where("tag_id", "=", $dotId)
                    ->pluck("offer_id")
                    ->toArray();
        return $links;
    }

    //Возвращает элементы только текущей категории
    private function getCategoryExclude($filter, $query) {
        if (empty($filter["category_id"])) return $query;
        $category_id = $filter["category_id"];
        $query = $query->join("amazon_offer_category_links", "amazon_offer_category_links.offer_id", "=", "amazon_offers.id")
                 ->where("amazon_offer_category_links.category_id", '=', $category_id);
        return $query;
    }

    //Производит дополнительный поиск по строке поиска
    private function getSearchQuery($filter, $query) {
        if (empty(trim($filter["search"]))) return $query;
        $search = $filter["search"];
        $query = $query->where('amazon_offers.title', 'like', "%$search%");

        return $query;
    }
}
