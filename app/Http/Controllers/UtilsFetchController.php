<?php

namespace App\Http\Controllers;

use App\LinkedCurrentTag;
use Illuminate\Http\Request;
use DB;
use App\InstagramPost;
use App\InstagramDot;
use App\AmazonOfferCategory;
use App\PostOfferLink;
use App\LogStatus;
use App\LogRow;
use App\AmazonOffer;
use Auth;
use Redirect;
use App\MassList;

class UtilsFetchController extends Controller
{
    public function status() {
        $before = DB::table("instagram_posts")->min("created");
        $after = DB::table("instagram_posts")->max("created");
        return response()->json(array(
            "before" => $before,
            "after" => $after,
        ));
    }

    public function statuses() {
        $statuses = config("appconfig.statuses");
        return response()->json($statuses);
    }

    public function startRange() {
        $data = array();
        $data["minDate"] = DB::table("instagram_posts")->min("created");
        $data["maxDate"] = DB::table("instagram_posts")->max("created"); 
        $data["range"] = [
            $data["minDate"],
            $data["maxDate"]
        ];
        return response()->json($data);
    }

    public function deleteOrphans(Request $request) {
        $codes = $request->get("codes");
        if (empty($codes)) $codes = [];
        DB::table('instagram_posts')->whereIn('code', $codes)->update(['status' => 404]);
        return response()->json([]);
    }

    public function deletePost($id = null) {
        if (empty($id)) return;
        $post = InstagramPost::where("code", "=", $id)->first();
        $post->deleted = true; 
        $post->save();
    }

    public function restorePost($id = null) {
        if (empty($id)) return;
        $post = InstagramPost::where("code", "=", $id)->first();
        $post->deleted = false; 
        $post->save();
    }

    public function saveDots($id = null, Request $request) {
        if (empty($id)) return;
        $dots = $request["dots"];
        if (empty($dots)) return;
        $post = InstagramPost::where("code", "=", $id)->first();
        $post_id = $post->id;
        foreach ($dots as &$dot) {
            $dot["post_id"] = $post_id;
        }
        InstagramDot::insert($dots);
        return;
    }


    public function deleteDot($id = null, Request $request) {
        if (empty($id)) return;
        InstagramDot::where("id", "=", $id)->delete();
    }

    public function saveDot($id = null, Request $request) {
        $dot = $request->get("dot");
        if (empty($id) || empty($dot)) return;
        $dotobj = InstagramDot::where("id", "=", $id)->first();
        if (empty($dotobj)) return;
        $dotobj->fill($dot);
        $dotobj->save();
    }

    public function deleteOfferlink($id) {
        //Добавляем инфомрацию в лог
        //$this->checkRemoveOfferLinkLog($id);

        //Проверяем статус поста
        //$this->checkRemoveOfferLinkStatus($id);

        PostOfferLink::where("id", "=", $id)->delete();
    }


    private function checkRemoveOfferLinkLog($id) {
        //Получаем объект связи
        $offer_link = PostOfferLink::where("id", "=", $id)->first();
        if (empty($offer_link)) return;
        $tag_id = $offer_link->tag_id;
        $offer_id = $offer_link->offer_id;

        //Получаем связанную точку
        $tag = InstagramDot::where("id", "=", $tag_id)->first();
        if (empty($tag)) return;
        $post_id = $tag->post_id;

        //Получаем связанный оффер
        $offer = AmazonOffer::where("id", "=", $offer_id)->first();
        if (empty($offer)) return;

        //Получаем данные оффера
        $offer_title = $offer->title;
        $offer_url = $offer->url;

        //Формируем данные для добавления
        $data = json_encode(array(
            "offer_title" => $offer_title,
            "offer_url" => $offer_url
        ));

        //Добавляем информацию
        $user = Auth::guard('api')->user();
        $user_id = $user->id;

        //Создаем новую запись лога
        $time = time();

        //Вставляем метку лога
        LogRow::insert([
            "user_id" => $user_id,
            "post_id" => $post_id,
            "status_id" => 3,
            "created" => $time,
            "data" => $data
        ]);
    }


    public function checkwork() {
        $dotId = 1;
        $dot = InstagramDot::where("id", "=", $dotId)->first();
        if (empty($dot)) return;

        $post_id = $dot->post_id;
        $ids = InstagramDot::where("post_id", "=", $post_id)->pluck("id")->toArray();
        $post_offer_links = PostOfferLink::whereIn("tag_id", $ids)->pluck("offer_id")->toArray();
        $post_offers = AmazonOffer::whereIn("id", $post_offer_links)->get()->toArray();

        //Смотрим есть - ли там невалидный оффер (привязанный невалидный оффер)
        $not_valid = false;
        foreach ($post_offers as $offer) {
            $valid = $offer["valid"];
            if ($valid) {
                $not_valid = true;
                break;
            }
        }

        $post = InstagramPost::where("id", "=", $post_id)->first();
        if (empty($post)) return;
        if ($not_valid) {
            $post->status = 666;
            $post->save();
            return;
        }

        //Иначе проверяем количество связанных постов
        $new_ids = InstagramDot::where("post_id", "=", $post_id)->pluck("id")->toArray();
        $post_offer_links = PostOfferLink::whereIn("tag_id", $new_ids)->pluck("offer_id")->toArray();
        if (count($post_offer_links >= 2) && $post->status != 30) {
            $post->status = 20;
            $post->save();
        }
    }


    private function checkRemoveOfferLinkStatus($id) {
        //Получаем ID поста. Смотрим есть - ли у него невалидные офферы в прявязке
        $link = PostOfferLink::where("id", "=", $id)->first();
        if (empty($link)) return;
        $target_post = $link->offer_id;
        $dot = InstagramDot::where("id", "=", $link->tag_id)->first();
        if (empty($dot)) return;
        $post_id = $dot->post_id;

        //Получаем список невалидных офферов
        $all_dots_ids = InstagramDot::where("post_id", "=", $post_id)->pluck("id")->toArray();
        $all_offers_ids = PostOfferLink::whereIn("tag_id", $all_dots_ids)->pluck("offer_id")->toArray();
        $not_valid = AmazonOffer::where("valid", "=", false)->where("id", "!=", $target_post)->whereIn("id", $all_offers_ids)->pluck("id")->toArray();

        //Если массив пуст - значит все стало валидным - меняем статус текущего поста. Не пуст - ничего не делаем
        if (empty($not_valid)) {

            //Проверяем количество привязанных офферов теперь. Если их меньше двух - то Editor, иначе Editor + Offers
            $total_dots = InstagramDot::where("post_id", "=", $post_id)->pluck("id")->toArray();
            $total_links = PostOfferLink::whereIn("tag_id", $total_dots)->where("offer_id", "!=", $target_post)->pluck("id")->toArray();
            if (count($total_links) >= 2) {
                $result_status = 30;
            } else {
                $result_status = 10;
            }

            $post = InstagramPost::where("id", "=", $post_id)->first();
            if (!empty($post)) {
                $post->status = $result_status;
                $post->save();
            }
        }
    }

    public function shortPost($id) {
        $offer = InstagramPost::where("id", "=", $id)->first();
        if (empty($offer)) return;
        $code = $offer->code;

        $link = "/navigator/".$code;
        return Redirect::to($link);
    }


    public function getOfferCategoryTree() {
        $trees = AmazonOfferCategory::with('children')
        ->whereNull('parent')
        ->get();

        $trees_array = $trees->toArray();
        return response()->json($trees_array);
    }

    public function getOfferCategoryTreeNode($id = null) {
        $case = "CASE WHEN child.id is NULL THEN 1 ELSE 0 END AS isLeaf"; 
        $trees = DB::table("amazon_offer_categories as element")
                     ->distinct()
                     ->select("element.id as id", "element.text as text", DB::raw($case))
                     ->where('element.parent', '=', $id)
                     ->leftJoin("amazon_offer_categories as child", 'element.id', '=', 'child.parent')
                     ->orderBy("element.text", "asc")
                     ->get();
        $trees_array = $trees->toArray();
        foreach($trees_array as &$item) {
            if ($item->isLeaf == "1") {
                $item->isLeaf = true;
            } else {
                $item->isLeaf = false;
            }
        }

        //Производим получение количества вложенных элементов для каждой категории
        foreach ($trees_array as &$item) {
            $id = $item->id;
            $count = 0;
            $count = DB::table('amazon_offer_category_links')->where("category_id", "=", $id)->groupBy('category_id')->count();
            $item->child_count = $count;
        }

        return response()->json($trees_array);
    }

    //Добавляет один элемент
    public function addNewOfferLink(Request $request) {
        $offerId = (int) $request->get("offerId");
        $dotId = (int) $request->get("dotId");

        //Создаем новый элемент
        $obj = new PostOfferLink();
        $obj->tag_id = $dotId;
        $obj->offer_id = $offerId;
        $obj->save();

        //Делаем логгирование
        //$this->checkAddOfferLinkLogs($offerId, $dotId);

        //Делаем проверку на статус
        //$this->checkAddOfferLinkStatus($offerId, $dotId);
    }


    //Добавляет список элементов
    public function addNewOfferLinkList(Request $request) {
        $list_id = $request->get("list_id");
        $dotId = $request->get("dotId");

        //Получаем список всех ID, связанных с текущим списком
        $ids = DB::table("category_offer_lists")
                ->select("offer_id")
                ->where("list_id", "=", $list_id)
                ->get();

        //В цикле создаем элементы
        foreach ($ids as $new_id) {
            $offerId = $new_id->offer_id;

            //Создаем новый элемент
            $obj = new PostOfferLink();
            $obj->tag_id = $dotId;
            $obj->offer_id = $offerId;
            $obj->save();

            //Делаем логгирование
            //$this->checkAddOfferLinkLogs($offerId, $dotId);

            //Делаем проверку на статус
            //$this->checkAddOfferLinkStatus($offerId, $dotId);
        }
    }


    //Функция, производящая логгирование при добавлении нового оффера
    private function checkAddOfferLinkLogs($offerId, $dotId) {
        //Получаем нужный пост
        $dot = InstagramDot::where("id", "=", $dotId)->first();
        if (empty($dot)) return;
        $post_id = $dot->post_id;

        //Получаем нужный оффер
        $offer = AmazonOffer::where("id", "=", $offerId)->first();
        if (empty($offer)) return;

        //Получаем данные оффера
        $offer_title = $offer->title;
        $offer_url = $offer->url;

        //Формируем данные для добавления
        $data = json_encode(array(
            "offer_title" => $offer_title,
            "offer_url" => $offer_url
        ));

        //Добавляем информацию
        $user = Auth::guard('api')->user();
        $user_id = $user->id;

        //Создаем новую запись лога
        $time = time();

        //Вставляем метку лога
        LogRow::insert([
            "user_id" => $user_id,
            "post_id" => $post_id,
            "status_id" => 2,
            "created" => $time,
            "data" => $data
        ]);
    }


    //Функция для проверки на статус
    private function checkAddOfferLinkStatus($offerId, $dotId) {
        $dot = InstagramDot::where("id", "=", $dotId)->first();
        if (empty($dot)) return;

        $post_id = $dot->post_id;
        $ids = InstagramDot::where("post_id", "=", $post_id)->pluck("id")->toArray();
        $post_offer_links = PostOfferLink::whereIn("tag_id", $ids)->pluck("offer_id")->toArray();
        $post_offers = AmazonOffer::whereIn("id", $post_offer_links)->get()->toArray();

        //Смотрим есть - ли там невалидный оффер (привязанный невалидный оффер)
        $not_valid = false;
        foreach ($post_offers as $offer) {
            $valid = $offer["valid"];
            if ($valid) {
                $not_valid = true;
                break;
            }
        }

        $post = InstagramPost::where("id", "=", $post_id)->first();
        if (empty($post)) return;
        if ($not_valid) {
            $post->status = 666;
            $post->save();
            return;
        }

        //Иначе проверяем количество связанных постов
        $new_ids = InstagramDot::where("post_id", "=", $post_id)->pluck("id")->toArray();
        $post_offer_links = PostOfferLink::whereIn("tag_id", $new_ids)->pluck("offer_id")->toArray();
        if (count($post_offer_links >= 2) && $post->status != 30) {
            $post->status = 20;
            $post->save();
        }
    }


    public function testAuth(Request $request) {
        echo "test";
    }


    //Функция возвращает список статусов логов
    public function getLogsStatuses() {
        $statuses = LogStatus::all()->pluck("message", "id");
        return $statuses;
    }

    //Функция возвращает список логов
    public function getLogs($code) {
        $post = InstagramPost::where("code", "=", $code)->first();
        if (empty($post)) return array();

        $id = $post->id;
        $statuses = LogRow::where("post_id", "=", $id)
            ->select('users.name as username', 'log_rows.*')
            ->join("users", "log_rows.user_id", "=", "users.id") 
            ->orderBy("created", "desc")
            ->get();
        return $statuses;
    }


    //Список невалидных офферов
    public function getInvalidOffers() {
        $not_valid = DB::table("amazon_offers")
                         ->select("amazon_offers.*")
                         ->join("post_offer_links", "amazon_offers.id", "=", "post_offer_links.offer_id")
                         ->where("amazon_offers.valid", "=", "0")
                         ->groupBy("amazon_offers.id")
                         ->get();

        foreach ($not_valid as &$item) {
            $item->path = json_decode($item->path);
            $item->images = json_decode($item->images);
        }

        return $not_valid;
    }


    //Для деактивации невалидного оффера на всех элементах
    public function deactivateOffer($id) {
        DB::table("post_offer_links")
            ->where("offer_id", "=", $id)->delete();
    }


    //Для замены нерабочего оффера
    public function replaceOffer($before, $after) {
        DB::table("post_offer_links")
            ->where("offer_id", "=", $before)
            ->update(["offer_id" => $after]); 
    }


    //Список существующих связей
    public function getGroupsList($id, $offer_id) {
        $result = DB::table("mass_lists")
                    ->select("mass_lists.id as id", "mass_lists.name as name", "category_offer_lists.list_id as joined")  
                    ->leftJoin('category_offer_lists', function($leftJoin) use($offer_id)
                    {
                        $leftJoin->on("mass_lists.id", "=", "category_offer_lists.list_id");
                        $leftJoin->on(DB::raw('category_offer_lists.offer_id'), DB::raw('='),DB::raw("'".$offer_id."'"));
                    })
                    ->where("mass_lists.category_id", "=", $id)
                    ->groupBy("mass_lists.id")
                    ->get();

        return $result;
    }


    //Отвязка элемента
    public function unlinkGroupsList($id) {
        DB::table("category_offer_lists")
            ->where("offer_id", $id)
            ->delete();
    }


    //Привязка элемента
    public function linkGroupsList($id, $list_id) {
        DB::table("category_offer_lists")
        ->insert([
            "offer_id" => $id,
            "list_id" => $list_id
        ]);
    }


    //Для возврата всех топ-списков
    public function getTopList($id) {
        return DB::table("mass_lists")
        ->where("category_id", $id)->get();
    }


    //Список существующих связей
    public function addGroupsList($name, $category_id, Request $request) {
        //Добавляем категорию
        $list = new MassList();
        $list->category_id = $category_id;
        $list->name = $name;
        $list->save();

        //Получаем id созданного списка
        $id = $list->id;

        //Добавляем для связи товар
        if ($id) {
            $offer_id = $request->get("offer_id");
            DB::table("category_offer_lists")->insert(array(
                "list_id" => $id,
                "offer_id" => $offer_id
            ));
        }
    }
}
