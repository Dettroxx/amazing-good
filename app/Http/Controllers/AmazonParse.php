<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Libraries\Amazon\AmazonParser;
use App\AmazonOffer;
use App\AmazonOfferCategory;
use App\AmazonOfferCategoryLink;
use App\Import\ImportAmazonPreview;
use DB;
use Response;


class AmazonParse extends Controller
{
    //Функция, которая выводит список ссылок для обновления товаров
    public function getUpdateOffers() {
        $max_to_update = config("appconfig.max_offer_update") ?? 500;
        $offers_to_update = DB::table("amazon_offers")
                              ->select("url")
                              ->limit($max_to_update)
                              ->orderBy("updated", "ASC")
                              ->pluck("url")
                              ->toArray();

        //Объединяем данные
        $data = implode("\r\n", $offers_to_update);
        return $data;
    }


    private function unique_offers() {
        return ImportAmazonPreview::select("offer_id")->distinct()->pluck("offer_id")->toArray();
    }

    private function exists_offers() {
        $unique = $this->unique_offers();
        return AmazonOffer::select("offer_id")->distinct()->whereIn("offer_id", $unique)->pluck("offer_id")->toArray();
    }

    private function new_offers() {
        $unique = $this->unique_offers();
        $exists = $this->exists_offers();
        return array_diff($unique, $exists);
    }

    private function removeNoValidOffers($no_valid) {
        ImportAmazonPreview::whereIn("offer_id", $no_valid)->delete();
    }

    private function filterOnlyValid($insert_import) {
        $result = array();
        $no_valid = array();
        foreach ($insert_import as $item) {
            $valid = $item["valid"];
            if (!$valid) {
                $no_valid[] = $item["offer_id"];
            } else {
                $result[] = $item;
            }
        }

        //Производим удаление невалидных офферов
        $this->removeNoValidOffers($no_valid);

        return $result;
    }

    private function insertNewOffers($ids) {
        $max_offer_parse = config("appconfig.max_offer_parse") ?? 10;
        $import = ImportAmazonPreview::whereIn("offer_id", $ids)->take($max_offer_parse)->get()->toArray();
        $prepared_import = $this->prepareImport($import);
        $selected_offer_ids = $this->getSelectedOfferIds($prepared_import);


        //Фильтрация похожих (одинаковых значений)
        $filtered_import = $this->afterFilter($prepared_import);

        //Подготовка данных для вставки
        $insert_import = $this->insertImport($filtered_import);

        //Отсеивание невалидных офферов
        $insert_import = $this->filterOnlyValid($insert_import);

        //Вставка данных
        AmazonOffer::insert($insert_import);

        //Производим генерацию категорий
        $this->generateCategories($insert_import);

        //Удаляем спарсенные элементы
        ImportAmazonPreview::whereIn("offer_id", $selected_offer_ids)->delete();
    }

    private function updateExistOffers($ids) {
        $max_offer_parse = config("appconfig.max_offer_parse") ?? 10;
        $import = ImportAmazonPreview::whereIn("offer_id", $ids)->take($max_offer_parse)->get()->toArray();
        $prepared_import = $this->prepareImport($import);
        $selected_offer_ids = $this->getSelectedOfferIds($prepared_import);

        //Фильтрация похожих (одинаковых значений)
        $filtered_import = $this->afterFilter($prepared_import);

        //Подготовка данных для вставки
        $insert_import = $this->insertImport($filtered_import, true);

        //Производим обновление данных
        $this->updateOffers($insert_import);

        //Производим обновление для "невалидных данных"
        $this->processNotValidUpdate($insert_import);

        //Производим удаление обновленных постов
        ImportAmazonPreview::whereIn("offer_id", $selected_offer_ids)->delete();
    }


    //Функция, которая производит обновление невалидных данных
    private function processNotValidUpdate($data) {
        $not_valid = array();
        foreach ($data as $item) {
            $valid = $item["valid"];
            if ($valid) continue;
            $not_valid[] = $item["offer_id"];
        }

        //Производим цикл работы над обновлением элементов
        $offers_ids = DB::table("amazon_offers")->whereIn("offer_id", $not_valid)->pluck("id")->toArray();
        $tag_links = DB::table("post_offer_links")->whereIn("offer_id", $offers_ids)->pluck("tag_id")->toArray();
        $post_ids = DB::table("instagram_dots")->whereIn("id", $tag_links)->pluck("post_id")->toArray();

        //Делаем посты - broken
        DB::table("instagram_posts")->whereIn("id", $post_ids)->update(array(
            "status" => 666
        ));
    }


    //Функция для проверки валидности статусов
    private function validateOffer($data) {
        $status = $data["status"];
        $price = trim($data['price']);
        $status = strtolower($status);

        //Проверка валидных статусов
        $in_stock = preg_match('/^in[\s]*stock.*/ui', $status, $matches, PREG_OFFSET_CAPTURE);
        $in_available = preg_match('/^available from.*/ui', $status, $matches, PREG_OFFSET_CAPTURE);
        if ($in_stock || $in_available) {
            return true;
        } else {
            if (!empty($price)) {
                return true;
            } else {
                return false;
            }
        }
    }


    private function updateOffers($data) {
        foreach ($data as $item) {
            $offer_id = $item["offer_id"];
            $element = AmazonOffer::where("offer_id", "=", $offer_id)->first();
            if (empty($element)) continue;

            //Иначе обновляем и производим проверку статусов
            $element->fill($item);
            $element->save();
        }
    }


    public function parseOffers() {
        //Список ids уже существующих офферов и список совершенно новых
        $new_offers_ids = $this->new_offers();
        $exist_offers_ids = $this->exists_offers();

        //Производим обработку данных
        $this->insertNewOffers($new_offers_ids);
        $this->updateExistOffers($exist_offers_ids);
    }


    //Производит генерацию категорий для вставленных элементов
    private function generateCategories($insert_import) {
        foreach ($insert_import as $item) {
            $element = AmazonOffer::where("offer_id", "=",$item['offer_id'])->first();
            if (empty($element)) continue;
            $this->prepareCategories($element);
        }
    }


    //Подготавливает данные для импорта
    private function insertImport($import, $need_update = false) {
        $result = array();
        foreach ($import as $item) {
            $element = array();
            $element["offer_id"] = $item["offer_id"];

            //Из - за особенностей
            if (!$need_update) {
                $element["path"] = json_encode($item["path"]);
                $element["images"] = json_encode($item["images"]);
                $element["thumbs"] = json_encode($item["thumbs"]);
                $element["sizes"] = json_encode($item["sizes"]);
                $element["colors"] = json_encode($item["colors"]);
            } else {
                $element["path"] = $item["path"];
                $element["images"] = $item["images"];
                $element["thumbs"] = $item["thumbs"];
                $element["sizes"] = $item["sizes"];
                $element["colors"] = $item["colors"];
            }

            $element["title"] = $item["title"];
            $element["stars"] = $item["stars"];


            $element["status"] = $item["status"];
            $element["content"] = $item["content"];
            $element["url"] = $item["url"];
            $element["price_current"] = $item["price"];
            $element["price_old"] = $item["old_price"];

            //Чекинг на статусы
            $isOfferValid = $this->validateOffer($item);
            $element["valid"] = $isOfferValid;

            //Если это обновление - дополнительно производим такое
            if (!$need_update) {
                $element["created"] = time();
            } else {
                $element["updated"] = time();
            }


            $result[] = $element;
        }

        return $result;
    }


    private function afterFilter($insert_import) {
        $result = array();
        foreach ($insert_import as $item) {
            $code = $item["offer_id"];
            $result[$code] = $item;
        }

        return array_values($result);
    }


    //Список Offer_ID которые мы используем
    private function getSelectedOfferIds($prepared_import) {
        $result = array();
        foreach ($prepared_import as $item) {
            $result[] = $item["offer_id"];
        }
        return $result;
    }


    //Начальная обработка данных с таблицы импорта
    private function prepareImport($import) {
        $result = array();
        foreach ($import as &$item) {
            $item["path"] = $this->addOneFilter(explode("|", $item["path"]));
            $item["stars"] = (double)$item['stars'];
            $item["price"] = (double)$item['price'];
            $item["old_price"] = (double)$item['old_price'];
            $item["images"] = $this->addOneFilter(explode("|", $item["images"]));
            $item["thumbs"] = $this->addOneFilter(explode("|", $item["thumbs"]));
            $item["sizes"] = $this->addOneFilter(explode("|", $item["sizes"]));
            $item["colors"] = $this->addOneFilter(explode("|", $item["colors"]));
            $result[] = $item;
        }
        return $result;
    }


    //Функция для дополнительной фильтрации массивных данных
    private function addOneFilter($arr) {
        $result = array();
        foreach ($arr as $item) {
            $item_element = trim($item);
            if (empty($item_element)) continue;
            $result[] = $item_element;
        }

        return $result;
    }


    //Производим обработку категорий
    private function prepareCategories($item) {
        $path = $item->path;
        if (empty($path) || !is_array($path)) return;

        $this->createCategoriesIfNotExist($path);
        $this->createCategoriesLinksIfNotExist($item, $path);
    }

    private function createCategoriesIfNotExist($path) {
        foreach ($path as $level => $name) {
            $this->createCategoryOne($level, $name, $path);
        }
    }

    private function createCategoryOne($level, $name, $path) {
        $category = AmazonOfferCategory::where("level", "=", $level)->where("text", "=", $name)->first();
        if ($category) return;

        //Если не корневой элемент
        $parent = null;
        if ($level > 0) {
            $parent_level = $level-1;
            $parent_name = $path[$parent_level];
            $parent_category = AmazonOfferCategory::where("level", "=", $parent_level)->where("text", "=", $parent_name)->first();
            if (!empty($parent_category)) {
                $parent = $parent_category->id;
            }
        }

        //Создаем элемент категории
        $obj = new AmazonOfferCategory();
        $obj->parent = $parent;
        $obj->level = $level;
        $obj->text = $name;
        $obj->save();
    }

    private function createCategoriesLinksIfNotExist($item, $path) {
        $item_id = $item->id;
        foreach ($path as $level => $name) {
            $category = AmazonOfferCategory::where("level", "=", $level)->where("text", "=", $name)->first();
            if (empty($category)) continue;
            $category_id = $category->id;
            $this->createCategoryLinksOne($item_id, $category_id);
        }
    }


    private function createCategoryLinksOne($item_id, $category_id) {
        $obj = new AmazonOfferCategoryLink();
        $obj->offer_id = $item_id;
        $obj->category_id = $category_id;
        $obj->save();
    }
}
