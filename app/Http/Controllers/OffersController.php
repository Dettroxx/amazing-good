<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InstagramPost;
use App\InstagramTagLink;
use App\PostOfferLink;
use DB;

class OffersController extends Controller
{
    public function index($id) {
        $post = InstagramPost::where("code", "=", $id)->first();
        if (empty($post)) return response()->json(array());

        $post_id = $post->id;
        $data = DB::table("instagram_dots")
            ->select([
                "post_offer_links.id as offerlink_id",
                "instagram_dots.*",
                "amazon_offers.*"
            ])
            ->where("post_id", "=", $post_id)
            ->join('post_offer_links', 'instagram_dots.id', '=', 'post_offer_links.tag_id')
            ->join('amazon_offers', 'post_offer_links.offer_id', '=', 'amazon_offers.id')
            ->get();

        return response()->json($data);
    }


    public function offersDot($tag_id) {
        $data = DB::table("post_offer_links")
            ->select([
                "post_offer_links.id as offerlink_id",
                "amazon_offers.*"
            ])
            ->where("tag_id", "=", $tag_id)
            ->join('amazon_offers', 'post_offer_links.offer_id', '=', 'amazon_offers.id')
            ->get();
        return response()->json($data);
    }
}
