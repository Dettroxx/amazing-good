<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\PostPreviewController;
use DB;
use App\User;

class StatController extends Controller
{
    public function status() {
        //Пользователи
        $users = DB::table("users")->select("name")->orderBy("id", "asc")->pluck("name");

        //Даты
        $start = (array)DB::table("instagram_posts")->select(DB::raw("MIN(created) as start"))->first();
        $end = (array)DB::table("instagram_posts")->select(DB::raw("MAX(created) as end"))->first();

        //Статусы
        $statuses = config("appconfig.statuses");

        //Формируем результирующие данные
        $data = array(
            "date" => array(
                "start" => $start["start"],
                "end" => $end["end"]
            ),
            "users" => $users,
            "statuses" => $statuses
        );
        return $data;
    }

    //Функция обработчик данных
    public function handle(Request $request) {
        //Пользователь
        $user = $request->get("user");
        if ($user == 'None') {
            return [
                'success' => false,
                'message' => 'Пользователь не выбран'
            ];
        } 

        //Даты
        $date = $request->get("date");

        //Получение информации
        $result = $this->getInfo($date['start'], $date['end'], $user);

        //Возврат даты
        return [
            'success' => true,
            'data' => [
                "user" => $user,
                "before" => $date['start'],
                "after" => $date['end'],
                "count" => $result
            ]
        ];
    }


    //Получение информации
    public function getInfo($before, $after, $user) {
        $user = User::where("name", "=", $user)->first();
        if (!$user) return [];
        $user_id = $user->id;

        //Информация
        $ready = $this->getReadyPosts($user_id, $before, $after);
        return $ready;
    }


    //Информация о фильтре
    public function getFilterQuery($user_id, $before, $after) {
        $base = DB::table("instagram_posts as Posts")
        ->select("Posts.id as id", 
                "Posts.created as created",
                 DB::raw("SUM(IF(DotLinks.id IS NOT NULL, 1, 0)) as offers"), 
                 DB::raw("SUM(IF(Offer.valid = 0, 1, 0)) as not_valid"),
                 DB::raw("SUM(IF(Posts.text IS NULL, 1, 0)) as text_null"),
                 DB::raw("SUM(IF(Posts.status = '321', 1, 0)) as deleted_status"),
                 DB::raw("SUM(IF(Log.id IS NOT NULL, 1, 0)) as log_rows")
        )
        ->leftJoin("log_rows as Log", "Posts.id", "=", "Log.post_id")
        ->leftJoin("instagram_dots as Dots", "Posts.id", "=", "Dots.post_id")
        ->leftJoin("post_offer_links as DotLinks", "Dots.id", "=", "DotLinks.tag_id")
        ->leftJoin("amazon_offers as Offer", "DotLinks.offer_id", "=", "Offer.id")
        ->where("Log.user_id", "=", $user_id)
        ->where("Log.created", ">=", $before)
        ->where("Log.created", "<=", $after)
        ->groupBy("Posts.id");
        
        return $base;
    }


    //Готовые посты
    public function getReadyPosts($user_id, $before, $after) {
        $query = $this->getFilterQuery($user_id, $before, $after);
        
        //Второй запрос
        $base = DB::table(DB::raw("({$query->toSql()}) as Filter"))
        ->mergeBindings($query)
        ->join("instagram_posts as Posts", "Filter.id", "=", "Posts.id")
        ->where("Filter.not_valid", "=", 0)
        ->where("Filter.offers", ">=", 2)
        ->whereNotNull('Posts.text')
        ->where("Posts.status", "!=", "321")
        ->orderBy("Posts.created", "desc")
        ->groupBy("Posts.id")
        ->get()
        ->count();

        return $base;
    }
}
