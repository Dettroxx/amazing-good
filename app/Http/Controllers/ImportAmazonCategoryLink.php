<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class ImportAmazonCategoryLink extends Controller
{
    public function getUpdateOffers() {
    	$links = DB::table("import_amazon_offer_links")->pluck("link")->toArray();
    	return implode("\r\n", $links);  
    }
}
