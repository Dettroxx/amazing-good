<?php

namespace App\Http\Controllers;

use App\InstagramPost;
use Illuminate\Http\Request;
use App\Http\Resources\PostPreviewResource;
use App\Http\Resources\PostResource;
use DB;

class PostPreviewController extends Controller
{
    public function index(Request $request) {
        $before = $request->get("before");
        $after = $request->get("after");
        $status = trim($request->get("status"));

        //Проверка на валидность статусов
        $allow_statuses = config("appconfig.allow_statuses");
        if (!in_array($status, $allow_statuses)) return; 

        //В зависимости от типа статуса, разный код
        switch ($status) {
            case 'NEW':
                $base = $this->getNewPosts(); 
                break;

            case 'EDITOR':
                $base = $this->getEditorPosts();
                break;

            case 'EDITOR_OFFERS':
                $base = $this->getEditorOffersPosts();
                break;

            case 'OFFERS':
                $base = $this->getOffersPosts();
                break;

            case 'READY':
                $base = $this->getReadyPosts();
                break;

            case 'TRASH':
                $base = $this->getTrashPosts();
                break;

            case 'BROKEN':
                $base = $this->getBrokenPosts();
                break;

            default:
                return array();
        }

        //Общие для всех элементы
        $base->where("Posts.created", ">=", $before);
        $base->where("Posts.created", "<=", $after);
        return json_encode($base->paginate(20));  
    }


    //Возвращает записи (Статус NEW) 
    public function getNewPosts() {
        $base = DB::table("instagram_posts as Posts")
        ->select("Posts.id as post_id", "Posts.code", "Posts.thumb", "Posts.created", "Posts.status")
        ->where("Posts.status", "=", 0) 
        ->where("Posts.deleted", "!=", true)
        ->inRandomOrder();

        return $base;
    }

    //Возвращает записи (Статус Editor) 
    public function getEditorPosts() {
        $base = DB::table("instagram_posts as Posts")
        ->select("Posts.id as post_id", "Posts.code", "Posts.thumb", "Posts.created", "Posts.status")
        ->where("Posts.status", "=", 700)
        ->where("Posts.deleted", "!=", true)
        ->orderBy("updated_at", "desc");

        return $base;
    }


    //Возвращает записи (Статус EditorOffers) 
    public function getEditorOffersPosts() {
        $base = DB::table("instagram_posts as Posts")
        ->select("Posts.id as post_id", "Posts.code", "Posts.thumb", "Posts.created", "Posts.status")
        ->where("Posts.status", "=", 800)
        ->where("Posts.deleted", "!=", true)
        ->orderBy("updated_at", "desc");

        return $base;
    }


    //Возвращает записи (Статус Offers) 
    public function getOffersPosts() {
        $base = DB::table("instagram_posts as Posts")
        ->select("Posts.id as post_id", "Posts.code", "Posts.thumb", "Posts.created", "Posts.status")
        ->where("Posts.status", "=", 900)
        ->where("Posts.deleted", "!=", true)
        ->orderBy("updated_at", "desc");

        return $base;
    }


    //Возвращает записи (Статус Ready) 
    public function getReadyPosts($select = null, $sort = 'Posts.updated_at', $orderby = 'asc') {
        if (!$select) {
            $select = array("Posts.id as post_id", "Posts.code", "Posts.thumb", "Posts.created", "Posts.status");
        }
        $base = DB::table("instagram_posts as Posts")
        ->select($select) 
        ->where("Posts.status", "=", 1000)
        ->where("Posts.deleted", "!=", true)
        ->orderBy($sort, $orderby); 

        return $base;
    }


    //Статус Trash
    public function getTrashPosts() {
        $base = DB::table('instagram_posts as Posts')
        ->select("Posts.id as post_id", "Posts.code", "Posts.thumb", "Posts.created", "Posts.status")
        ->where("Posts.deleted", "=", true)  
        ->orderBy("Posts.updated_at", "desc");

        return $base;
    }


    //Возвращает записи (Статус Broken) 
    public function getBrokenPosts() {
        $base = DB::table("instagram_posts as Posts")
        ->select("Posts.id as post_id", "Posts.code", "Posts.thumb", "Posts.created", "Posts.status")
        ->where("Posts.status", "=", 1200)
        ->where("Posts.deleted", "!=", true)
        ->orderBy("updated_at", "desc");

        return $base;
    }

    
    //Возвращает базовый подзапрос фильтра
    public function getFilterQuery($sortby = null, $orderby = null) {
        $base = DB::table("instagram_posts as Posts")
        ->select("Posts.id as id", 
                "Posts.image_ok as image_ok",
                "Posts.created as created",
                 DB::raw("SUM(IF(DotLinks.id IS NOT NULL, 1, 0)) as offers"), 
                 DB::raw("SUM(IF(Offer.valid = 0, 1, 0)) as not_valid"),
                 DB::raw("SUM(IF(Posts.text IS NULL, 1, 0)) as text_null"),
                 DB::raw("SUM(IF(Posts.deleted = 1, 1, 0)) as deleted_status"), 
                 DB::raw("SUM(IF(Log.id IS NOT NULL, 1, 0)) as log_rows")
        )
        ->leftJoin("log_rows as Log", "Posts.id", "=", "Log.post_id")
        ->leftJoin("instagram_dots as Dots", "Posts.id", "=", "Dots.post_id")
        ->leftJoin("post_offer_links as DotLinks", "Dots.id", "=", "DotLinks.tag_id")
        ->leftJoin("amazon_offers as Offer", "DotLinks.offer_id", "=", "Offer.id");
        $base->groupBy("Posts.id"); 
   
        //Сортировка 
        if ($sortby == 'ID') {
            $base->orderBy("Posts.id", $orderby ?: 'asc');
        } else {
            $base->orderBy("Posts.created", "desc");     
        }

        
        
        return $base;
    }


    //Функция для получения данным по статусам
    public function getStatusStatistic($before=null,$after=null,$id=null) {
        $query = $this->getFilterQuery();

        //Второй запрос
        $base = DB::table("instagram_posts")
        ->select(
            DB::raw("SUM(IF(status = 0, 1, 0)) as NEW"),
            DB::raw("SUM(IF(status = 700, 1, 0)) as EDITOR"),
            DB::raw("SUM(IF(status = 800, 1, 0)) as EDITOR_OFFERS"),
            DB::raw("SUM(IF(status = 900, 1, 0)) as OFFERS"),
            DB::raw("SUM(IF(status = 1000, 1, 0)) as READY"),
            DB::raw("SUM(IF(deleted = 1, 1, 0)) as TRASH"),  
            DB::raw("SUM(IF(status = 1200, 1, 0)) as BROKEN")
        );

        //Для показа дат
        if (!empty($before)) {
            $base->where("created", ">=", $before);
        }

        if (!empty($after)) {
            $base->where("created", "<=", $after);
        }

        if (!empty($id)) {
            $base->where("id", "=", $id); 
        }
        
        $status_result = (array)$base->first();
        return $status_result;
    }


    public function statuses(Request $request) {
        $statuses = config("appconfig.statuses");
        $before = (integer) $request->get("before");
        $after = (integer) $request->get("after");
        $statistic = $this->getStatusStatistic($before,$after);
        
        //Получаем список связанных со статусами элементов
        foreach ($statuses as &$status) {
            $value = trim($status["value"]);
            if (isset($statistic[$value])) {
                $status["stat"] = $statistic[$value];
            }
        }

        
        return response()->json($statuses);
    }

    public function test() {
        $result = (array) $this->getReadyPosts(array(
            "Posts.id as id",
            "Log.user_id as user" 
        ))
        ->join("log_rows as Log", "Posts.id", "=", "Log.post_id")
        ->get()
        ->toArray();
        
        foreach ($result as $item) {
            $id = $item->id;
            $author = $item->user;
            
            InstagramPost::where("id", "=", $id)->update(['author' => $author]);
        }

        echo "<pre>";
        print_r($result);
        echo "</pre>";
    }

    public function post($id) {
        $post = InstagramPost::where("code", '=', $id)->first();
        if (!$post->current_title) {
            $post->current_title = $post->caption;
        }

        $statuses = $this->getStatusStatistic(null, null, $post->id); 
        $current_status = "NEW";
        foreach ($statuses as $key => $value) {
            if ($value) {
                $current_status = $key;
                break;
            }
        }

        $post->statuses = $current_status;
        return new PostResource($post);
    }
}
