<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Controllers\PostPreviewController;
use View;

class TelegramExport extends Controller
{
    public function TelegramSend() {
        $stats = $this->getBaseStat();
        $offers_count = $this->getOffersCount();
        $view = View::make('telegram_stat', ['stat' => $stats, 'offers_count' => $offers_count]);
        $contents = $view->render();

        //Отправка сообщения в телеграм
        $this->sendTelegramMessage($contents);
    }


    protected function getOffersCount() {
        $result = (array) DB::table("amazon_offers")
        ->select(DB::raw("COUNT(*) as count"))
        ->get()
        ->toArray();

        return $result[0]->count;
    }


    //Функция, которая получает статистику
    public function getBaseStat() {
        $stat_controller = new PostPreviewController();
        $stats = $stat_controller->getStatusStatistic();
        return $stats;
    }

    //Отправка сообщения на сервер telegram
    protected function sendTelegramMessage($message) {
        $botToken = env('TELEGRAM_TOKEN');
        $chatID = env('TELEGRAM_CHAT');
        $proxy = env('TELEGRAM_PROXY_IP');
        $port = env('TELEGRAM_PROXY_PORT');
        $password = env('TELEGRAM_PROXY_PASSWORD');


        $website="https://api.telegram.org/bot".$botToken;
        $params=[
            'chat_id'=>$chatID,
            'text'=> $message,
        ];
        $ch = curl_init($website . '/sendMessage');

        //Настройка прокси
        curl_setopt($ch, CURLOPT_PROXY, $proxy);
        curl_setopt($ch, CURLOPT_PROXYPORT, $port);
        curl_setopt($ch, CURLOPT_PROXYUSERPWD, $password);
        curl_setopt($ch, CURLOPT_PROXYTYPE, CURLPROXY_SOCKS5);

        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, ($params));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        curl_close($ch);
    }
}
