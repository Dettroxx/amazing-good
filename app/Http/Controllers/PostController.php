<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\PostResource;
use App\InstagramPost;
use DB;
use App\InstagramTagLink;
use App\LinkedCurrentTag;
use App\InstagramDot;
use Auth;
use App\LogRow;

class PostController extends Controller
{
    public function index($id) {
        $post = InstagramPost::where("code", '=', $id)->first();
        if (!$post->current_title) {
            $post->current_title = $post->caption;
        }
        return new PostResource($post);
    }

    public function saveData($id, Request $request) {
        $post = InstagramPost::where("code", "=", $id)->first();
        if (empty($post)) return;

        //Производим действия по сохранению определенных элементов
        $this->simpleSave($post, $request);

        //Сохраняем связанные теги
        $this->saveLinkedTags($post, $request);

        //Сохраняем ресурс
        $post->status = 0;  
        $post->save();

        //Производим генерацию лога 
        //$this->saveLogs($post); 

        //Производим работу по статутизации и логировании
        //$this->preparePostStatus($post, $request); 
    }

    private function saveLogs($post) {
        $id = $post->id;
        $user = Auth::guard('api')->user();
        $user_id = $user->id;

        //Создаем новую запись лога
        $time = time();

        //Вставляем метку лога
        LogRow::insert([
            "user_id" => $user_id,
            "post_id" => $id,
            "status_id" => 1,
            "created" => $time
        ]);
    }

    private function simpleSave($post, $request) {
        $text = $request->get("text");
        $current_title = $request->get("title");
        $inout = $request->get("inout");
        $current_tags = $request->get("current_tags");

        $post->text = $text;
        $post->inout = $inout;
        $post->current_title = $current_title;
        $post->current_tags = $current_tags;
    }

    private function saveLinkedTags($post, $request) {
        $id = $post->id;
        LinkedCurrentTag::where("post_id", "=", $id)->delete();
        $linkedtags = $request->get("linkedtags");
        if (empty($linkedtags)) $linkedtags = array();
        foreach ($linkedtags as $category) {
            $tags = $category["tags"];
            if (empty($tags)) continue;

            $values = array();
            foreach ($tags as $tag) {
                $values[] = array(
                    "post_id" => $id,
                    "tag_id" => $tag["value"]
                );
            }

            //Производим вставку тегов
            LinkedCurrentTag::insert($values);
        }
    }

    private function preparePostStatus($post, $request) {
        $status = $post->status;
        if ($status == 321) return;

        $editor_was = $this->checkEditor($post, $request);
        $offers_was = $this->checkOffers($post, $request);
        if ($editor_was && $offers_was) {
            $post->status = 30;
        } else if ($editor_was) {
            $post->status = 10;
        } else if ($offers_was) {
            $post->status = 20;
        }

        $post->save();
    }

    //Проверка редактора
    private function checkEditor($post, $request) {
        $id = $post->id;
        $current_tags = $post->current_tags;
        $inout = $post->inout;
        $caption = $post->caption;
        $title = $post->current_title;
        $text = $post->current_text;
        $count = LinkedCurrentTag::where("post_id", "=", $id)->count();

        $editor_was = false;
        if (!empty($current_tags) || $inout > 0 || (!empty($title) && $title != $caption) || !empty($text) || $count > 0) {
            $editor_was = true;
        }

        return $editor_was;
    }

    //Проверка офферов
    private function checkOffers($post, $request) {
        $id = $post->id;
        $count = InstagramDot::where("post_id", "=", $id)->count();
        if ($count == 0) {
            return false;
        } else {
            return true;
        }
    }
}
