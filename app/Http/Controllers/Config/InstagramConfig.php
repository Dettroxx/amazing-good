<?php

namespace App\Http\Controllers\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\InstagramAccount;
use App\InstagramConfigTag;

class InstagramConfig extends Controller
{
    public function loadAccounts() {
        return InstagramAccount::all();
    }

    public function addAccount(Request $request) {
        $data = $request->toArray();
        InstagramAccount::insert($data);
    }

    public function deleteAccount($id) {
        InstagramAccount::where("id", "=", $id)->delete();
    }

    public function changeAccount($id, Request $request) {
        $account = InstagramAccount::where("id", "=", $id)->first();
        if (empty($account)) return;
        $data = $request->toArray();
        $account->fill($data);
        $account->save();
    }

    public function loadTags($type) {
        $data = InstagramConfigTag::where("type", '=', $type)->orderBy("name", "asc")->get();
        return $data;
    }

    public function changeTag($id, Request $request) {
        $tag = InstagramConfigTag::where("id", "=", $id)->first();
        if (empty($tag)) return;
        $data = $request->toArray();
        $tag->fill($data);
        $tag->save();
    }

    public function addTag(Request $request) {
        $data = $request->toArray();
        InstagramConfigTag::insert($data);
    }

    public function deleteTag($id) {
        InstagramConfigTag::where("id", "=", $id)->delete();
    }
}
