<?php

namespace App\Http\Controllers\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\AmazonCategoryConfig;

class AmazonCategoryConfigController extends Controller
{
	public function getUpdateCategories() {
		$categories = DB::table('amazon_category_configs')->pluck("category")->toArray();
		$categories = implode("\r\n", $categories);
		return $categories;
	}

    public function load() {
    	$categories = DB::table('amazon_category_configs')->pluck("category")->toArray();
    	return array(
    		"categories" => implode("\r\n", $categories),
    	);
    }

    private function prepare($raw_data, $column) {
    	$data = explode("\n", $raw_data);
    	$new_data = array();
    	foreach ($data as $item) {
    		$el = trim($item);
    		if (empty($item)) continue;
    		$new_data[] = array(
    			$column => $item
    		);
    	}

    	return $new_data;
    }

    public function save(Request $request) {
    	$categories = $request->get("categories");
    	$categories = $this->prepare($categories, "category");
    	DB::table('amazon_category_configs')->delete();
    	DB::table('amazon_category_configs')->insert($categories);
    }
}
