<?php

namespace App\Http\Controllers\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Proxy;
use App\UserAgent;
use DB;

class ProxyController extends Controller
{
	public function getUpdateProxies() {
		$proxies = DB::table('proxies')->pluck("proxy")->toArray();
		$proxies = implode("\r\n", $proxies);
		return $proxies;
	}

	public function getUpdateAgents() {
		$useragent = DB::table('user_agents')->pluck("usergent")->toArray();
		$useragent = implode("\r\n", $useragent);
		return $useragent;
	}

    public function load() {
    	$proxies = DB::table('proxies')->pluck("proxy")->toArray();
    	$agents = DB::table('user_agents')->pluck("usergent")->toArray();

    	return array(
    		"proxies" => implode("\n", $proxies),
    		"agents" => implode("\n", $agents)
    	);
    }

    private function prepare($raw_data, $column) {
    	$data = explode("\n", $raw_data);
    	$new_data = array();
    	foreach ($data as $item) {
    		$el = trim($item);
    		if (empty($item)) continue;
    		$new_data[] = array(
    			$column => $item
    		);
    	}

    	return $new_data;
    }

    public function save(Request $request) {
    	$proxies = $request->get("proxies");
    	$agents = $request->get("agents");

    	//Разбивка
    	$proxies = $this->prepare($proxies, "proxy");
    	$agents = $this->prepare($agents, "usergent");

    	//Очищение таблиц
    	DB::table('proxies')->delete();
    	DB::table('user_agents')->delete();

    	//Вставка
    	DB::table('proxies')->insert($proxies);
    	DB::table('user_agents')->insert($agents);
    }
}
