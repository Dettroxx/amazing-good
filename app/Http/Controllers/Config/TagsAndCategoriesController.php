<?php

namespace App\Http\Controllers\Config;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\TagCategory;
use App\Tag;

class TagsAndCategoriesController extends Controller
{
    public function loadCetegories() {
        $categories = TagCategory::orderBy("order", 'desc')->orderBy('id', 'asc')->get();
        return $categories;
    }


    public function tags($id) {
        $tags = Tag::where("category_id", "=", $id)->get();
        return $tags;
    }


    public function categoryAdd(Request $request) {
        TagCategory::insert($request->toArray());
    }


    public function categoryDelete($id) {
        TagCategory::where('id', '=', $id)->delete();
    }


    public function categorySave($id, Request $request) {
        $category = TagCategory::where('id', '=', $id)->first();
        $category->fill($request->toArray());
        $category->save();
    }


    public function tagSave($id, Request $request) {
        $category = Tag::where('id', '=', $id)->first();
        $category->fill($request->toArray());
        $category->save();
    }


    public function tagAdd(Request $request) {
        Tag::insert($request->toArray());
    }

    public function tagDelete($id) {
        Tag::where('id', '=', $id)->delete();
    }


    public function categoryChangeOrder($id, Request $request) {
        $tag = TagCategory::where("id", "=", $id)->first();
        if (empty($tag)) return;
        $order = (int) $request->get("order");

        //Вычисления
        if ($order < 0) {
            $order = 0;
        } else if ($order > 255) {
            $order = 255;
        }

        //Добавляем в элемент
        $tag->order = $order;
        $tag->save();

        return $order;
    }
}
