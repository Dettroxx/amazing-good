<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use DB;
use Illuminate\Support\Facades\Hash;

class AdminToolsController extends Controller
{
    public function generateUsers() {
        $last = DB::table("users")
        ->orderBy("id", "desc")
        ->limit(1)
        ->pluck("id");
        

        $start = $last[0]+1;
        $count = $start + 50;
        for ($i = $start; $i < $count; $i++) {
            $pass = Hash::make("passapp".$i);
            $pass = base64_encode($pass);
            $pass = substr($pass,-8,8);
            $name = "user".$i;

            echo $name.";".$pass;
            echo "<br/>";

            User::create([
                'name' => 'user'.$i,
                'email' => 'user'.$i.'@app.ru',
                'password' => Hash::make($pass)
            ]);
        }
    }
}
