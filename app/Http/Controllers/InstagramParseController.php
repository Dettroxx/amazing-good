<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InstagramPost;
use App\Libraries\Instagram\InstagramParse;
use App\Libraries\Instagram\InstagramConfig;
use DB;
use App\Import\ImportInstagramPreview;
use App\InstagramConfigTag;

class InstagramParseController extends Controller
{
    //Для списка обновления тегов
    public function getUpdateTags() {
        $tags = InstagramConfigTag::where("type", "=", "0")->pluck("name")->toArray();

        $tags_links = array();
        foreach ($tags as $tag) {
            $tags_links[] = "https://www.instagram.com/explore/tags/".$tag;
        }

        $tags = implode("\r\n", $tags_links);
        return $tags;
    }

    public function parsePosts() {
        $unique = ImportInstagramPreview::select("code")->distinct()->pluck("code")->toArray(); 
        $exists = InstagramPost::select("code")->distinct()->whereIn("code", $unique)->pluck("code")->toArray();
        $onlyresult = array_diff($unique, $exists);


        //Выборка значений что создаем
        $import = ImportInstagramPreview::whereIn("code", $onlyresult)->get()->toArray();
        
        $prepared_import = $this->prepareImport($import);

        //Отбираем теперь элементы, которые нам по тегам не подходят
        $filtered_import = $this->filterImport($prepared_import);

        //Производим подготовку данных для вставки в базу данных
        $insert_import = $this->insertImport($filtered_import);

        //Производим вставку элементов в базу данных
        $after_filtration = $this->afterFilter($insert_import);

        //Производим вставку
        if (!empty($after_filtration)) {
            InstagramPost::insert($after_filtration);
        }
        

        //Удаляем все элементы
        ImportInstagramPreview::truncate();
    }


    //Конечный фильтр
    private function afterFilter($insert_import) {
        $result = array();
        foreach ($insert_import as $item) {
            $code = $item["code"];
            $result[$code] = $item;
        }

        return array_values($result);
    }


    //Подготавливает данные для вставки в уже существующую таблицу данных
    private function insertImport($import) {
        $results = array();
        foreach ($import as $item) {
            //Теги, которые были спарсены
            $parsed_tags = array_merge(array($item["tag_first"]), $item["selected_tags"]);
            $parsed_tags = array_unique($parsed_tags);

            //Изображения
            $count = count($item["images"]);
            $first = 0;
            $last = $count-1;
            $thumb = $item["images"][$first];
            $image = $item["images"][$last];

            //Формируем данные
            $result = array(
                "caption" => $item["caption"],
                "code" => $item["code"],
                "current_title" => $item["caption"],
                "url" => $item["url"],
                "created" => time(),
                "parse_tags" => json_encode($parsed_tags),
                "image" => $image,
                "thumb" => $thumb,
                "status" => 0, //901 - будет для постов не обработанных на комменты
            );
            $results[] = $result;
        }
        return $results;
    }


    //Фильтрация по вторичным тегам
    private function filterImport($import) {
        $tags = InstagramConfigTag::where("type", "=", "1")->pluck("name")->toArray();
        $result = array();
        foreach ($import as &$item) {
            $image = $item["images"];
            if (empty($image)) continue;  

            $item_tags = $item["tags"];
            $selected_tags = array();

            foreach ($tags as $tag) {
                foreach ($item_tags as $item_tag) {
                    $tag = strtolower(trim($tag));
                    $item_tag = strtolower(trim($item_tag));
                    if ($tag == $item_tag) $selected_tags[] = $tag;
                }
            }

            if (empty($selected_tags)) continue;
            $item["selected_tags"] = $selected_tags;
            $result[] = $item;
        }

        return $result;
    }


    //Обработка данных с таблицы импорта
    private function prepareImport($import) {
        $result = array();
        foreach ($import as &$item) {
            $images = trim($item["images"]);
            if (empty($images)) continue;

            $item["tags"] = explode("|", $item["tags"]);
            $item["images"] = explode("|", $item["images"]);
            $result[] = $item;
        }
        return $result;
    }
}
