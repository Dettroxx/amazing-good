<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use View;
use DB;
use App\Http\Controllers\PostPreviewController;

class WPExport extends Controller 
{
    //Экспорт списка продуктов
    public function products(Request $request) {
        $start = $request->get("start") ?: 0;
        $limit = $request->get("limit") ?: 2000;
        $sort = $request->get("sort") ?: 'updated_at';
        $orderby = $request->get("orderBy") ?: 'desc'; 

        //Выборка
        $items = DB::table("amazon_offers")
        ->where("valid", "=", "1")
        ->orderBy($sort, $orderby)
        ->skip($start)
        ->limit($limit)
        ->get()
        ->toArray();

    	//Вносим результирующие значения
    	$data = array(
    		"items" => $items
    	);
    	$contents = View::make('wp_export_products', $data);
        return response($contents)->header('Content-Type', 'application/rss+xml');
    }


    //Экспорт списка фильтров
    public function filters(Request $request) {
        $start = $request->get("start") ?: 0;
        $limit = $request->get("limit") ?: 2000;
        $sort = $request->get("sort") ?: 'updated_at';
        $orderby = $request->get("orderBy") ?: 'desc'; 

        //Получаем список базовых категорий
        $categories = (array)DB::table("tag_categories")
        ->skip($start)
        ->limit($limit)
        ->get()
        ->toArray();

        //Получаем список связанных тегов
        $tags = (array)DB::table("tags")
        ->select(array(
            "tags.id as tag_id",
            "tags.name as tag_name",
            "tag_categories.id as category_id",
            "tag_categories.name as category_name"
        ))
        ->join('tag_categories', 'tags.category_id', '=', 'tag_categories.id')
        ->get()
        ->toArray();   

        $data = array(
            "categories" => $categories,
            "tags" => $tags
        );
        $contents = View::make('wp_export_filters', $data);
        return response($contents)->header('Content-Type', 'application/rss+xml');
    }


    //Для выгрузки единиц контента
    public function items(Request $request, PostPreviewController $controller) {
        $start = $request->get("start") ?: 0;
        $limit = $request->get("limit") ?: 2000;
        $sort = $request->get("sort") ?: 'updated_at';
        $orderby = $request->get("orderBy") ?: 'desc'; 

        //Получаем список инстаграм постов
        $posts = $controller->getReadyPosts([
            "Posts.*",
            DB::raw("User.name as author"),
            DB::raw("GROUP_CONCAT(DISTINCT tags.name ORDER BY tags.id ASC SEPARATOR '|') AS categories"),
            DB::raw("GROUP_CONCAT(DISTINCT instagram_dots.id ORDER BY instagram_dots.id ASC SEPARATOR ',') AS collections_ids")
        ], 'Posts.'.$sort, $orderby) 
        ->leftJoin("users as User", "Posts.author", "=", "User.id")  
        ->leftJoin('linked_current_tags', 'Posts.id', '=', 'linked_current_tags.post_id')
        ->leftJoin('tags', 'linked_current_tags.tag_id', '=', 'tags.id')
        ->join('instagram_dots', 'Posts.id', '=', 'instagram_dots.post_id')
        ->groupBy("Posts.id")
        ->skip($start)
        ->limit($limit)
        ->get()
        ->toArray(); 


        //Выводим результат
        $data = array(
            "inout_config" => array(
                "v1" => "interior",
                "v2" => "exterior",
                "v3" => "both",
                "v0" => "none",
            ), 
            "posts" => $posts
        );
        $contents = View::make('wp_export_items', $data);
        return response($contents)->header('Content-Type', 'application/rss+xml');
    }


    //Для экспорта списка тегов "коллекций" для прикрепления
    public function collections(Request $request) {
        $start = $request->get("start") ?: 0;
        $limit = $request->get("limit") ?: 2000;
        $sort = $request->get("sort") ?: 'updated_at';
        $orderby = $request->get("orderBy") ?: 'desc'; 

        $dots = (array)DB::table("instagram_dots")
                ->select(
                    "instagram_dots.*",
                    DB::raw("GROUP_CONCAT(DISTINCT post_offer_links.offer_id ORDER BY post_offer_links.id ASC SEPARATOR ',') AS offers_ids")
                )
                ->join('post_offer_links', 'instagram_dots.id', '=', 'post_offer_links.tag_id')
                ->groupBy("instagram_dots.id")
                ->skip($start)
                ->limit($limit)
                ->get()
                ->toArray(); 

        $data = array(
            'items' => $dots
        );
        $contents = View::make('wp_export_collections', $data);
        return response($contents)->header('Content-Type', 'application/rss+xml');
    }


    public function test(Request $request, PostPreviewController $controller) {
        $start = $request->get("start") ?: 0;
        $limit = $request->get("limit") ?: 2000;
        $sort = $request->get("sort") ?: 'updated_at';
        $orderby = $request->get("orderBy") ?: 'desc'; 

        //Получаем список инстаграм постов
        $posts = (array)$controller->getReadyPosts()
        ->select(
            "Posts.*",
            DB::raw("MAX(log_rows.created) as updated_date"),
            DB::raw("MAX(users.name) as author"),
            DB::raw("GROUP_CONCAT(DISTINCT tags.name ORDER BY tags.id ASC SEPARATOR '|') AS categories"),
            DB::raw("GROUP_CONCAT(DISTINCT instagram_dots.id ORDER BY instagram_dots.id ASC SEPARATOR ',') AS collections_ids")
        )
        ->leftJoin('linked_current_tags', 'Posts.id', '=', 'linked_current_tags.post_id')
        ->join('log_rows', 'Posts.id', '=', 'log_rows.post_id')
        ->leftJoin('users', 'log_rows.user_id', '=', 'users.id')
        ->leftJoin('tags', 'linked_current_tags.tag_id', '=', 'tags.id')
        ->join('instagram_dots', 'Posts.id', '=', 'instagram_dots.post_id')
        ->orderBy("updated_date", "desc") 
        ->groupBy("Posts.id") 
        ->skip($start)
        ->limit($limit)
        ->get()
        ->toArray(); 

        echo count($posts);
    }


    //Обрабатывает изображения
    public function Instamage() {
        $images = $this->GetImagesList();
        
        foreach ($images as $id => $image) {
            try {
                $check = $this->downloadImageGetWidth($image);
            } catch (\Exception $e) {
                $status_code = $e->getResponse()->getStatusCode();
                DB::table("instagram_posts")
                ->where("id", "=", $id)
                ->delete();
            }
            
            if ($check) {
                DB::table("instagram_posts")
                ->where("id", "=", $id)
                ->update([
                    "image_ok" => true
                ]);
            }
            sleep(2);
        }
    }


    //Получаем список изображений
    protected function GetImagesList() {
        $images = (array)
        DB::table("instagram_posts")
        ->where("image_ok", "=", false)
        ->inRandomOrder()
        ->limit(10)
        ->pluck('image', 'id')
        ->toArray();

        return $images;
    }


    //Скачивает изображение и возвращает его ширину
    protected function downloadImageGetWidth($url) {
        $path = storage_path().'/tempimg.jpg';
        $client = new \GuzzleHttp\Client();
        $client->get($url, ['save_to' => $path]); 
        
        $info = getimagesize($path);
        if (count($info) > 0) {
            $val = (int) $info[0];
            if ($val < 640) {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    }
}
