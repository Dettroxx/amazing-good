<?php

namespace App\Http\Resources;

use App\InstagramPost;
use App\LinkedCurrentTag;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\InstagramDotResource;
use App\InstagramDot;
use App\PostOfferLink;
use App\InstagramComment;
use App\InstagramTagLink;
use App\TagCategory;
use DB;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = array();
        $data["data"] =  parent::toArray($request);
        $data["linkedtags"] = $this->getLinkedTags();
        $data["comments"] = $this->getComments();
        $data["tags"] = $this->getTags();
        $data["tagsAvailable"] = $this->getTagsAvailable();
        $data["dots"] = $this->getLinkedDots();
        $data["total_links"] = $this->getTotalLinks();
        $data["statuses"] = $this->statuses;
        return $data;
    }

    private function getLinkedDots() {
        $dots = InstagramDot::where("post_id", '=', $this->id)->get();
        return InstagramDotResource::collection($dots);
    }

    private function getTotalLinks() {
        $dots = InstagramDot::where("post_id", '=', $this->id)->pluck("id")->toArray();
        $links = PostOfferLink::whereIn("tag_id", $dots)->get();
        return InstagramDotLinkResource::collection($links);
    }

    private function getComments() {
        $comments = InstagramComment::where("post_id", "=", $this->id)->get();
        return InstagramCommentResource::collection($comments);
    }

    private function getTags() {
        $tags = InstagramTagLink::where("post_id", "=", $this->id)->get();
        return InstagramTagLinkResource::collection($tags);
    }

    private function getTagsAvailable() {
        return TagsResource::groupedTags();
    }

    private function getLinkedTags() {
        //Получаем список всех доступных теговых категорий
        $categories = DB::table("tag_categories")->orderBy("order", "desc")->orderBy('id', 'asc')->get();
        $categories = $this->getLinkedTagsLinks($categories, $this->id);
        return $categories->toArray();

        $tags = LinkedCurrentTag::where("post_id", "=", $this->id)->get();
        return LinkedCurrentTagResource::collection($tags);
    }

    //Возвращает список категорий с уже прикрепленными тегами
    private function getLinkedTagsLinks($categories, $post_id) {
        foreach ($categories as &$category) {
            $category_id = $category->id;

            //Получаем список связанных с текущей категорией тегов
            $tags = DB::table("tags")
                        ->select("tags.id as value", "tags.name as label")
                        ->where("category_id", "=", $category_id)
                        ->join("linked_current_tags", "linked_current_tags.tag_id", "=", "tags.id")
                        ->where("linked_current_tags.post_id", "=", $post_id)
                        ->get()->toArray();

            if (empty($tags)) {
                $category->tags = array();
            } else {
                $category->tags = $tags;
            }
        }

        return $categories;
    }
}
