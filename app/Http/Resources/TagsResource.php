<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Tag;

class TagsResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }

    public static function groupedTags() {
        $tags = Tag::all();
        $data = array();
        foreach ($tags as $tag) {
            $category_id = $tag["category_id"];
            $tag = self::prepareOneTag($tag);
            $data['c'.$category_id][] = $tag;
        }
        return $data;
    }

    private static function prepareOneTag($tag) {
        return array(
            "label" => $tag["name"],
            "value" => $tag["id"]
        );
    }
}
