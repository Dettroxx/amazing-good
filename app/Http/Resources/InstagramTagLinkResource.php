<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\InstagramTag;

class InstagramTagLinkResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);
        $data["tag"] = $this->getTag();
        return $data;
    }

    private function getTag() {
        $tag = InstagramTag::where("id", '=', $this->tag_id)->get();
        return new InstagramTagResource($tag);
    }
}
