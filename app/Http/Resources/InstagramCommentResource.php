<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\InstagramUser;

class InstagramCommentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);
        $data["user"] = $this->getLinkedUser();
        return $data;
    }

    private function getLinkeduser() {
        $user = InstagramUser::where("id", '=', $this->user_id)->first();
        return new InstagramUserResource($user);
    }
}
