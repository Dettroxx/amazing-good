<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Tag;
use App\TagCategory;

class LinkedCurrentTagResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }


    public static function collection($tags) {
        $data = self::groupCollection($tags);
        return $data;
    }


    private static function groupCollection($tags) {
        $tags_names = self::getTags($tags);
        $categories = self::getTagsCategories($tags_names);
        $grouped_tags = self::groupByTag($tags);
        $grouped_tag_names = self::groupByCategory($tags_names, $grouped_tags);
        $total_grouped = self::totalGroup($categories, $grouped_tag_names);
        return $total_grouped;
    }


    private static function totalGroup($categories, $grouped_tag_names) {
        $data = array();
        foreach ($categories as $category) {
            $category_id = $category["id"];
            $el = $grouped_tag_names[$category_id];
            $category["tags"] = $el;
            $data[] = $category;
        }
        return $data;
    }


    private static function groupByCategory($tag_names, $grouped_tags) {
        $data = array();
        foreach ($tag_names as $tag_name) {
            $category_id = $tag_name["category_id"];
            $data[$category_id][] = array(
                "label" => $tag_name["name"],
                "value" => $tag_name["id"]
            );
        }
        return $data;
    }


    private static function groupByTag($tags) {
        $data = array();
        foreach ($tags as $tag) {
            $tag_id = $tag["tag_id"];
            $data[$tag_id] = $tag;
        }
        return $data;
    }


    private static function getTags($tags) {
        $ids = $tags->pluck("tag_id");
        $tags = Tag::whereIn("id", $ids)->get();
        return $tags;
    }

    private static function getTagsCategories($tags) {
        $category_ids = $tags->pluck("category_id");
        $categories = TagCategory::whereIn("id", $category_ids)->get();
        return $categories;
    }
}
