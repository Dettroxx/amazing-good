<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\PostOfferLink;

class InstagramDotResource extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $data = parent::toArray($request);
        $data["offer_links"] = $this->getLinkedPosts();
        return $data;
    }

    private function getLinkedPosts() {
        $links = PostOfferLink::where("tag_id", '=', $this->id)->get();
        return InstagramDotLinkResource::collection($links);
    }
}
