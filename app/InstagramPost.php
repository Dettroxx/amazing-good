<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
use Auth;

class InstagramPost extends Model
{
    protected $casts = [
        'current_tags' => 'array',
        'parse_tags' => 'array',
        'deleted' => 'boolean',
        'inout' => 'integer'
    ];

    public function setOptionsAttribute($options)
    {
        $this->attributes['parse_tags'] = json_encode($options);
    }

    protected $fillable = [
        'id', 'code', 'caption', 'url', 'thumb', 'image', 'text', 'parse_tags', 'created', 'deleted', 'commented', 'current_tags', 'current_title', 'freeze_current_title', 'inout', 'status', 'status_change', 'author'
    ];


    //События, выполняемые при сохранении модели
    public static function boot()
    {
        parent::boot();

        self::saving(function($model){ 
            //Получение базового фильтра
            $filter = self::getFilter($model->id);

            //Определение статуса для поста
            self::detectStatus($filter, $model);
        });

        self::updating(function($model){ 
            //Получение базового фильтра
            $filter = self::getFilter($model->id);

            //Определение статуса для поста
            self::detectStatus($filter, $model);
        });
    }

    //Получение фильтра поста
    public static function getFilter($id) {
        $base = (array) DB::table("instagram_posts as Posts")
        ->select("Posts.id as id", 
                "Posts.deleted as deleted",
                 DB::raw("COUNT(Offer.id) as offers"), 
                 DB::raw("SUM(IF(Offer.valid = 0, 1, 0)) as not_valid"),
                 DB::raw("SUM(IF(Posts.text IS NULL, 1, 0)) as text_null")
        ) 
        ->where("Posts.id", "=", $id)
        ->leftJoin("instagram_dots as Dots", "Posts.id", "=", "Dots.post_id")
        ->leftJoin("post_offer_links as DotLinks", "Dots.id", "=", "DotLinks.tag_id")
        ->leftJoin("amazon_offers as Offer", "DotLinks.offer_id", "=", "Offer.id")
        ->groupBy("Posts.id")
        ->first();

        return $base;
    }


    //Определение статуса поста
    public static function detectStatus($filter, $model) {
        $deleted = false; 
 
        //Статус Editor
        if (!$filter['not_valid'] && $filter['offers'] >= 2 && $filter['text_null']) {
            $status = 700;
        }

        //Статус EditorOffers
        else if (!$filter['not_valid'] && $filter['offers'] < 2 && $filter['text_null']) {
            $status = 800;
        }

        //Статус Offers
        else if (!$filter['not_valid'] && $filter['offers'] < 2 && !$filter['text_null']) { 
            $status = 900;
        }

        //Статус Ready
        else if (!$filter['not_valid'] && $filter['offers'] >= 2 && !$filter['text_null']) {
            $status = 1000;
        }

        //Статус Broken 
        else if ($filter['not_valid']) {
            $status = 1200; 
        }  
   
        $model->author = Auth::guard('api')->user()->id;  
        $model->status = $status;  
    }
}
