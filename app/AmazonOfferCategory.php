<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AmazonOfferCategory extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'level', 'text', 'parent'
    ];

    public function children_one()
    {
        return $this->hasMany(AmazonOfferCategory::class, 'parent');
    }


    public function children()
    {
        return $this->children_elements()->with('children');
    }
}
