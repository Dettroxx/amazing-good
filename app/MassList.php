<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MassList extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id', 'category_id' 
    ]; 
}
