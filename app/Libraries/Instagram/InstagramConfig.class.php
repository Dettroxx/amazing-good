<?php

namespace App\Libraries\Instagram;
use App\InstagramAccount;
use App\InstagramConfigTag;

//Класс, инкапсулирующий в себе конфигурацию
class InstagramConfig {
    private $config_name;
    private $tags2 = [];
    private $accounts = [];
    private $maxComments = 0;

    //Возвращает текущий тег (для парсинга)
    public function getCurrentTag() {
        $first_tag = InstagramConfigTag::where("type", 0)->orderByRaw("RAND()")->first()->toArray();
        if (empty($first_tag)) return null;
        $name = $first_tag["name"];
        return $name;
    }


    /**
     * @return string[] Возвращает список вторичных тегов
     */
    public function getSecondTags() {
        return $this->tags2;
    }

    /**
     * @return mixed[] Возвращает один аккаунт для авторизации пользователя в инстраграм
     */
    public function getCurrentAccount() {
        $account = InstagramAccount::orderByRaw("RAND()")->first()->toArray();
        return $account;
    }


    /*
     * Возвращает максимальное количество комментов для парсинга
     */
    public function getMaxComments() {
        return $this->maxComments;
    }

    /**
     * Создает конфигурацию для парсера instagram
     * @param mixed[] $config
     */
    public function __construct($configname) {
        $this->config_name = $configname;
        $this->tags2 = InstagramConfigTag::where("type", 1)->get()->pluck("name")->toArray();
        $this->accounts = $this->getConfig("accounts", array());
        $this->maxComments = $this->getConfig("maxComments");
    }


    //Функция - фасад для получения конфигурации
    private function getConfig($name, $default = false) {
        $data = config($this->config_name.".".$name);
        if (empty($data)) {
            if ($default) {
                return $default;
            } else {
                return $data;
            }
        } else {
            return $data;
        }
    }
}