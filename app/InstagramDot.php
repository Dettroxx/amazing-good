<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramDot extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'x', 'y', 'width', 'icon'
    ];
}
