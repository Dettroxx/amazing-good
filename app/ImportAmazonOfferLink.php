<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImportAmazonOfferLink extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'link'
    ];
}
