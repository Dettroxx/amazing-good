<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramPostStatus extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'name',
    ];
}
