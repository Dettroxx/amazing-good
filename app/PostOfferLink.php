<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostOfferLink extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'tag_id', 'offer_id'
    ];
}
