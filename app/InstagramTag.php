<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramTag extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id', 'tag'
    ];
}
