<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InstagramConfigTag extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'type', 'name', 'strict'
    ];
}
