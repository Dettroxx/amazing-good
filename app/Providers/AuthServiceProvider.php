<?php

namespace App\Providers;

use Illuminate\Support\Facades\Gate;
use Laravel\Passport\Passport;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Carbon\Carbon;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        Passport::routes();

        //Из - за того, что используется unix timestamp расширенный, то addDays не добавляет реальное время. Нужно умножать на 1000
        $expireAt = now()->addDays(60000);
        Passport::tokensExpireIn($expireAt);
        Passport::refreshTokensExpireIn($expireAt); 
    }
}
