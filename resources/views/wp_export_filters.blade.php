@php
echo '<?xml version="1.0" encoding="UTF-8" ?>';
@endphp
<!-- generator="WordPress/4.9.6" created="2018-06-01 14:11" -->
<rss version="2.0"
	xmlns:excerpt="http://wordpress.org/export/1.2/excerpt/"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:wp="http://wordpress.org/export/1.2/"
>

<channel>
	<generator>https://wordpress.org/?v=4.9.6</generator>
	<wp:wxr_version>1.2</wp:wxr_version>

	@foreach ($categories as $item)
		<wp:term>
			<wp:term_taxonomy><![CDATA[projects_category]]></wp:term_taxonomy>
			<wp:term_slug><![CDATA[c{{$item->id}}]]></wp:term_slug>
			<wp:term_name><![CDATA[{{$item->name}}]]></wp:term_name>
			<wp:term_description><![CDATA[]]></wp:term_description>
		</wp:term>
	@endforeach

	@foreach ($tags as $item)
		<wp:term>
			<wp:term_taxonomy><![CDATA[projects_category]]></wp:term_taxonomy>
			<wp:term_slug><![CDATA[t{{$item->tag_id}}]]></wp:term_slug>
			<wp:term_parent><![CDATA[{{$item->category_name}}]]></wp:term_parent>
			<wp:term_name><![CDATA[{{$item->tag_name}}]]></wp:term_name>
			<wp:term_description><![CDATA[]]></wp:term_description>
		</wp:term>
	@endforeach
</channel>
</rss>
