@php
echo '<?xml version="1.0" encoding="UTF-8" ?>';
@endphp
<rss version="2.0"
	xmlns:excerpt="http://wordpress.org/export/1.2/excerpt/"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:wp="http://wordpress.org/export/1.2/"
>

<channel>
	<generator>https://wordpress.org/?v=4.9.6</generator>
	<wp:wxr_version>1.2</wp:wxr_version>

	@foreach ($items as $item)
		<item>
			<!-- @data Название коллекции -->
			<title>{{ $item->id }}</title>
			<dc:creator><![CDATA[admin]]></dc:creator>
			<description></description>
			<content:encoded><![CDATA[]]></content:encoded>
			<excerpt:encoded><![CDATA[]]></excerpt:encoded>
			<!-- @data Текущая дата -->
			<wp:post_date><![CDATA[{{ date("Y-m-d H:i:s", time())}}]]></wp:post_date>
			<!-- @data Текущая дата по Гринвичу -->
			<wp:post_date_gmt><![CDATA[{{ gmdate("Y-m-d H:i:s", time())}}]]></wp:post_date_gmt>
			<wp:comment_status><![CDATA[closed]]></wp:comment_status>
			<wp:ping_status><![CDATA[closed]]></wp:ping_status>
			<!-- @data Алиас товара, можно убрать, будет транслитерация из названия -->
			<!-- <wp:post_name><![CDATA[peterborough22]]></wp:post_name> -->
			<wp:status><![CDATA[publish]]></wp:status>
			<wp:post_parent>0</wp:post_parent>
			<wp:menu_order>0</wp:menu_order>
			<wp:post_type><![CDATA[products_collection]]></wp:post_type>
			<wp:post_password><![CDATA[]]></wp:post_password>
			<wp:is_sticky>0</wp:is_sticky>
			<wp:postmeta>
				<wp:meta_key><![CDATA[products]]></wp:meta_key>
				<!-- @data Алиас XML_IDs продуктов через запятую -->
				<wp:meta_value><![CDATA[{{ $item->offers_ids }}]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[_products]]></wp:meta_key>
				<wp:meta_value><![CDATA[field_5b184b28a2f85]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[coords]]></wp:meta_key>
				<!-- @data Координаты относительно картинки в формате oX,oY -->
				<wp:meta_value><![CDATA[{{ round($item->x, 2) }},{{ round($item->y, 2) }}]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[_coords]]></wp:meta_key>
				<wp:meta_value><![CDATA[field_5b18dd529821e]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[xml_id]]></wp:meta_key>
				<!-- @data XML_ID коллекции в ПО, поле должно быть уникальным -->
				<wp:meta_value><![CDATA[{{ $item->id }}]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[_xml_id]]></wp:meta_key>
				<wp:meta_value><![CDATA[field_5b2bec8c89c04]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[size]]></wp:meta_key>
				<!-- @data Размер области метки -->
				<wp:meta_value><![CDATA[{{ round($item->width, 2) }}]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[_size]]></wp:meta_key>
				<wp:meta_value><![CDATA[field_5b33adea2b3d4]]></wp:meta_value>
			</wp:postmeta>
		</item>
	@endforeach
</channel>
</rss>
