@php
echo '<?xml version="1.0" encoding="UTF-8" ?>';
@endphp

<!-- generator="WordPress/4.9.6" created="2018-06-21 13:22" -->
<rss version="2.0"
	xmlns:excerpt="http://wordpress.org/export/1.2/excerpt/"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:wp="http://wordpress.org/export/1.2/"
>

<channel>
	<generator>https://wordpress.org/?v=4.9.6</generator>
	<wp:wxr_version>1.2</wp:wxr_version>


	@foreach ($posts as $post)
		<item>
			<!-- @data Название товара -->
			<title>{{ $post->current_title }}</title>
			<dc:creator><![CDATA[admin]]></dc:creator>
			<description></description>
			<!-- @data Текст под фотографией -->
			<content:encoded><![CDATA[{{ $post->text }}]]></content:encoded>
			<excerpt:encoded><![CDATA[]]></excerpt:encoded> 
			<!-- @data Текущая дата -->
			<wp:post_date><![CDATA[{{ $post->updated_at }}]]></wp:post_date> 
			<!-- @data Текущая дата по Гринвичу -->
			<wp:post_date_gmt><![CDATA[{{ $post->updated_at }}]]></wp:post_date_gmt>
			<wp:comment_status><![CDATA[open]]></wp:comment_status>
			<wp:ping_status><![CDATA[closed]]></wp:ping_status>
			<!-- @data Уникальный алиас -->
			<wp:post_name><![CDATA[{{ $post->code }}]]></wp:post_name>
			<wp:status><![CDATA[publish]]></wp:status>
			<wp:post_parent>0</wp:post_parent>
			<wp:menu_order>0</wp:menu_order>
			<wp:post_type><![CDATA[projects]]></wp:post_type>
			<wp:post_password><![CDATA[]]></wp:post_password>
			<wp:is_sticky>0</wp:is_sticky>

			@foreach (explode("|", $post->categories) as $tag) 
				<category domain="projects_category" nicename="{{ strtolower($tag) }}"><![CDATA[{{ $tag }}]]></category>
			@endforeach
			

			@foreach (json_decode($post->parse_tags, true) as $tag) 
				<category domain="projects_tag" nicename="{{ strtolower($tag) }}"><![CDATA[{{ $tag }}]]></category>
			@endforeach
			
			<wp:postmeta>
				<wp:meta_key><![CDATA[bailey_sidebar_position]]></wp:meta_key>
				<wp:meta_value><![CDATA[one-right-sidebar]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[collections]]></wp:meta_key>
				<!-- @data XML_IDs коллекций через запятую -->

				<wp:meta_value><![CDATA[{{ $post->collections_ids }}]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[_collections]]></wp:meta_key>
				<wp:meta_value><![CDATA[field_5b18460be995e]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[in-out]]></wp:meta_key>
				<!-- @data Выбор среди вариантов interior|exterior|both, если будет какое-то другое значение. Поле останется невыбранным -->

				@php
					if (isset($inout_config["v".$post->inout])) {
						$result_inout = $inout_config["v".$post->inout];
					} else {
						$result_inout = "none";
					}
				@endphp
				<wp:meta_value><![CDATA[{{ $result_inout }}]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[_in-out]]></wp:meta_key>
				<wp:meta_value><![CDATA[field_5b2b7fb276666]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[img_path]]></wp:meta_key>
				<!-- @data Ссылка на картинку -->
				<wp:meta_value><![CDATA[{{ $post->image }}]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[_img_path]]></wp:meta_key>
				<wp:meta_value><![CDATA[field_5b2bd9e81b15c]]></wp:meta_value>
			</wp:postmeta>

			<wp:postmeta>
				<wp:meta_key><![CDATA[author]]></wp:meta_key>
				<!-- @data Ник автора -->
				<wp:meta_value><![CDATA[{{$post->author}}]]></wp:meta_value>
			</wp:postmeta>
			<wp:postmeta>
				<wp:meta_key><![CDATA[_author]]></wp:meta_key>
				<wp:meta_value><![CDATA[field_5b55a7e0c10b7]]></wp:meta_value>
			</wp:postmeta>
		</item>
	@endforeach
</channel>
</rss>