@php
echo '<?xml version="1.0" encoding="UTF-8" ?>';
@endphp
<rss version="2.0"
	xmlns:excerpt="http://wordpress.org/export/1.2/excerpt/"
	xmlns:content="http://purl.org/rss/1.0/modules/content/"
	xmlns:wfw="http://wellformedweb.org/CommentAPI/"
	xmlns:dc="http://purl.org/dc/elements/1.1/"
	xmlns:wp="http://wordpress.org/export/1.2/"
>
	<channel>
		<generator>https://wordpress.org/?v=4.9.6</generator> 
		<wp:wxr_version>1.2</wp:wxr_version>

		@foreach ($items as $item)
		  	<item>
				<title>{{$item->title}}</title>
				<dc:creator><![CDATA[admin]]></dc:creator>
				<description></description>
				<content:encoded><![CDATA[]]></content:encoded>
				<excerpt:encoded><![CDATA[]]></excerpt:encoded>
				<!-- @data Текущая дата -->

				@php
					$updated = $item->updated;
					$created = $item->created;

					if (!$updated) {
						$thetime = $created;
					} else {
						$thetime = $updated;
					}
				@endphp
				<wp:post_date><![CDATA[{{ date("Y-m-d H:i:s", $thetime)}}]]></wp:post_date>
				<!-- @data Текущая дата по Гринвичу -->
				<wp:post_date_gmt><![CDATA[{{ gmdate("Y-m-d H:i:s", $thetime)}}]]></wp:post_date_gmt>
				<wp:comment_status><![CDATA[closed]]></wp:comment_status>
				<wp:ping_status><![CDATA[closed]]></wp:ping_status>
				<!-- @data Алиас товара, можно убрать, будет транслитерация из названия -->
				<wp:post_name><![CDATA[{{ $item->offer_id }}]]></wp:post_name>
				<wp:status><![CDATA[publish]]></wp:status>
				<wp:post_parent>0</wp:post_parent>
				<wp:menu_order>0</wp:menu_order>
				<wp:post_type><![CDATA[products]]></wp:post_type>
				<wp:post_password><![CDATA[]]></wp:post_password>
				<wp:is_sticky>0</wp:is_sticky>
				<wp:postmeta>
					<wp:meta_key><![CDATA[img_path]]></wp:meta_key>
					<!-- @data Путь к картинке -->
					<wp:meta_value><![CDATA[{{ (json_decode($item->images, true))[0] }}]]></wp:meta_value>
				</wp:postmeta>
				<wp:postmeta>
					<wp:meta_key><![CDATA[_img_path]]></wp:meta_key>
					<wp:meta_value><![CDATA[field_5b2bd96a69ea8]]></wp:meta_value>
				</wp:postmeta>
				<wp:postmeta>
					<wp:meta_key><![CDATA[link]]></wp:meta_key>
					<!-- @data Ссылка на товар -->
					<wp:meta_value><![CDATA[{{ $item->url }}?&tag=interiorseye-20]]></wp:meta_value>
				</wp:postmeta>
				<wp:postmeta>
					<wp:meta_key><![CDATA[_link]]></wp:meta_key>
					<wp:meta_value><![CDATA[field_5b2bd9a074507]]></wp:meta_value>
				</wp:postmeta>
				<wp:postmeta>
					<wp:meta_key><![CDATA[xml_id]]></wp:meta_key>
					<!-- @data XML_ID из ПО, поле должно быть уникальным -->
					<wp:meta_value><![CDATA[{{ $item->id }}]]></wp:meta_value>
				</wp:postmeta>
				<wp:postmeta>
					<wp:meta_key><![CDATA[_xml_id]]></wp:meta_key>
					<wp:meta_value><![CDATA[field_5b2bddea8e9fa]]></wp:meta_value>
				</wp:postmeta>
			</item>
		@endforeach
	</channel>
</rss>