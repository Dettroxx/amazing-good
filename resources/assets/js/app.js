import 'vue-material/dist/vue-material.min.css';
import 'vue-material/dist/theme/default.css';
import 'v-calendar/lib/v-calendar.min.css';


//Для логина через web-api токены
Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue')
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue')
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue')
);

var VueTruncate = require('vue-truncate-filter');
import Vue from 'vue';
import VueMaterial from 'vue-material';
import VueRouter from 'vue-router';
import Auth from './packages/auth/Auth.js';
var moment = require('vue-moment/vue-moment');
Vue.use(moment);
Vue.use(VueTruncate); 

import VCalendar from 'v-calendar';
Vue.use(VCalendar, {
    firstDayOfWeek: 2, 
    locale: 'ru-RU',
    datePickerTintColor: '#448aff',
    popoverVisibility: 'visible',
    popoverExpanded: true
});


import App from './views/App';
import PageNavigator from './pages/PageNavigator/PageNavigator';
import PagePosts from './pages/PagePosts/PagePosts';
import PageOffers from './pages/PageOffers/PageOffers';
import PageTools from './pages/PageTools/PageTools';
import PageConfig from './pages/PageConfig/PageConfig';
import PageLogin from './pages/PageLogin/PageLogin';
import PageCabinet from './pages/PageCabinet/PageCabinet';
import PageOfferReplace from './pages/PageOfferReplace/PageOfferReplace'; 
import PageStat from './pages/PageStat/PageStat'; 

Vue.use(VueRouter);
Vue.use(VueMaterial);
Vue.use(Auth);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/',
            redirect: '/navigator'
        },
        {
            path: '/cabinet',
            name: 'cabinet',
            component: PageCabinet,
            meta: {
                forAuth: true
            }
        },
        {
            path: '/login',
            name: 'login',
            component: PageLogin,
            meta: {
                forVisitors: true
            }
        },
        {
            path: '/navigator',
            name: 'navigator',
            component: PageNavigator,
            meta: {
                forAuth: true
            }
        },{
            path: '/navigator/:id',
            name: 'navigator-element',
            component: PageNavigator,
            meta: {
                forAuth: true
            }
        },
        {
            path: '/posts',
            name: 'posts',
            component: PagePosts,
            meta: {
                forAuth: true
            }
        },
        {
            path: '/offers',
            name: 'offers',
            component: PageOffers,
            meta: {
                forAuth: true
            }
        },
        {
            path: '/tools',
            name: 'tools',
            component: PageTools,
            meta: {
                forAuth: true
            }
        },
        {
            path: '/config',
            name: 'config',
            component: PageConfig,
            meta: {
                forAuth: true
            }
        },
        {
            path: '/offers-replace',
            name: 'offers-replace',
            component: PageOfferReplace,
            meta: {
                forAuth: true
            }
        },
        {
            path: '/stat',
            name: 'stat',
            component: PageStat,
            meta: {
                forAuth: true
            } 
        },
    ]
});


//Задаем настройки для axios;
import axios from 'axios';
axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest',
    'Authorization': 'Bearer '+Vue.auth.getToken(),
    'Accept': 'application/json',
};
axios.interceptors.response.use(response => {
    return response;
}, error => {
    if (error.response && error.response.status == 401) {
        Vue.auth.destroyToken();
        window.location.reload();
        return new Promise(() => {});
    } else {
        return error;
    }
});
Vue.prototype.$http = axios;

//Для выполнения любой своей функции до роутинга
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.forVisitors)) {
        if (Vue.auth.isAuthenticated()) {
            next({
                path: '/navigator'
            });
        } else next();
    } else if (to.matched.some(record => record.meta.forAuth)) {
        if (!Vue.auth.isAuthenticated()) {
            next({
                path: '/login'
            });
        } else {
            next();
        }
    }
});


const app = new Vue({
    el: '#app',
    components: {
        App,
    },
    router
});