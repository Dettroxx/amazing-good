<?php
return [
    "statuses" => [[
        "label" => "New",
        "value" => "NEW",
    ],[
        "label" => "Editor",
        "value" => "EDITOR"
    ],[
        "label" => "Editor+Offers",
        "value" => "EDITOR_OFFERS"
    ],[
        "label" => "Offers",
        "value" => "OFFERS"
    ],[
        "label" => "Ready",
        "value" => "READY"
    ],[
        "label" => "Trash",
        "value" => "TRASH"
    ],[
        "label" => "Broken",
        "value" => "BROKEN"
    ]],
    "allow_statuses" => [
        "NEW", "EDITOR", "EDITOR_OFFERS", "OFFERS", "READY", "TRASH", "BROKEN"
    ],
    "dots" => [

    ],
    "max_offer_parse" => 500,
    "max_offer_update" => 500
];