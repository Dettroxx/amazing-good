<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryOfferListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('category_offer_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->integer("list_id")->unsigned();
            $table->integer("offer_id")->unsigned();
            $table->index("list_id");
            $table->index("offer_id");
            $table->foreign('list_id')->references('id')->on('mass_lists')->onDelete('cascade');
            $table->foreign('offer_id')->references('id')->on('amazon_offers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('category_offer_lists');
    }
}
