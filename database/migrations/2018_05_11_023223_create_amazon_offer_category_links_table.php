<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmazonOfferCategoryLinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_offer_category_links', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('offer_id')->unsigned();
            $table->integer('category_id')->unsigned();

            $table->index(['offer_id']);
            $table->index(['category_id']);

            $table->foreign('offer_id')->references('id')->on('amazon_offers')->onDelete('cascade');
            $table->foreign('category_id')->references('id')->on('amazon_offer_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amazon_offer_category_links');
    }
}
