<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class FixInstagramImages extends Migration
{
    public function up()
    {
        Schema::table('instagram_posts', function (Blueprint $table) {
            $table->boolean('image_ok')->default(false);
        }); 
    }

    public function down()
    {
        Schema::table('instagram_posts', function (Blueprint $table) {
            $table->dropColumn('image_ok'); 
        });
    }
}
