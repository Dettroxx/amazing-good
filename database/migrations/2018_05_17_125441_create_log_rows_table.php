<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLogRowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_rows', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('post_id')->unsigned();
            $table->integer("user_id")->unsigned();
            $table->integer("status_id")->unsigned();
            $table->integer("created")->unsigned();
            $table->text("data")->nullable();

            $table->index("post_id");
            $table->index("user_id");
            $table->index("status_id");
            $table->index("created");

            $table->foreign('status_id')->references('id')->on('log_statuses')->onDelete('cascade');
            $table->foreign('post_id')->references('id')->on('instagram_posts')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_rows');
    }
}
