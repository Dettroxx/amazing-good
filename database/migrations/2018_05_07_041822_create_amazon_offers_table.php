<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmazonOffersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_offers', function (Blueprint $table) {
            $table->increments('id');
            $table->string('offer_id', 20)->nullable();
            $table->text('path')->nullable();
            $table->text('title')->nullable();
            $table->float('stars', 3, 2)->default(0);
            $table->text('images')->nullable();
            $table->text('thumbs')->nullable();
            $table->double('price_current')->default(0);
            $table->double('price_old')->default(0);
            $table->double('price_discount')->default(0);
            $table->string('status', 255)->nullable();
            $table->integer('status_code')->unsigned()->default(0);
            $table->text('sizes')->nullable();
            $table->text('colors')->nullable();
            $table->text("content")->nullable();

            $table->boolean('parsed')->default(false);
            $table->text('url')->nullable();
            $table->integer("created")->default(0);
            $table->integer("updated")->default(0);
            $table->boolean('valid')->default(true);

            $table->index(['id']);
            $table->index(['offer_id']);
            $table->index(['created']);
            $table->index(['parsed']);
            $table->index(['status_code']);
            $table->index(['valid']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amazon_offers');
    }
}
