<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstagramPostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instagram_posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code', 16)->unique();
            $table->text('caption');
            $table->string('url', 50);
            $table->text('thumb');
            $table->text('image');
            $table->text('text')->nullable();
            $table->integer("created")->unsigned();
            $table->boolean("deleted")->default(false);
            $table->boolean("commented")->default(false);
            $table->text('parse_tags')->nullable();
            $table->text('current_tags')->nullable();
            $table->text('current_title')->nullable(); 
            $table->boolean('freeze_current_title')->default(false);
            $table->integer('inout')->unsigned()->default(0);
            $table->integer("status")->unsigned()->default(0);
            $table->integer("status_change")->unsigned()->default(0);

            $table->index(['id']);
            $table->index(['code']);
            $table->index(['created']);
            $table->index(['deleted']);
            $table->index(['commented']);
            $table->index(['inout']);
            $table->index(['status']);
            $table->index(['status_change']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instagram_posts');
    }
}
