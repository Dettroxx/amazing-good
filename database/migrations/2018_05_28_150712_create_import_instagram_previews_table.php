<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportInstagramPreviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_instagram_previews', function (Blueprint $table) {
            $table->string("code", 100);
            $table->text("caption")->nullable();
            $table->text("images");
            $table->text("url");
            $table->text("tags")->nullable();
            $table->text("tag_first")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_instagram_previews');
    }
}
