<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class InstagramPostTimestamps extends Migration
{
    public function up()
    {
        Schema::table('instagram_posts', function (Blueprint $table) {
            $table->timestamps();
        }); 
    }

    public function down()
    {
        Schema::table('instagram_posts', function (Blueprint $table) {
            $table->dropColumn('updated_at'); 
            $table->dropColumn('created_at');  
        });
    }
}
