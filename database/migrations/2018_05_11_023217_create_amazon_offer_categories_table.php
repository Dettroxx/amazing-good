<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmazonOfferCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amazon_offer_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('level')->unsigned();
            $table->integer('parent')->unsigned()->nullable();
            $table->string('text', 255);

            $table->index(['level']);
            $table->index(['parent']);

            $table->foreign('parent')->references('id')->on('amazon_offer_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amazon_offer_categories');
    }
}
