<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImportAmazonPreviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('import_amazon_previews', function (Blueprint $table) {
            $table->string("offer_id", 100);
            $table->text("path");
            $table->text("title");
            $table->text("stars")->nullable();
            $table->text("images")->nullable();
            $table->text("thumbs")->nullable();
            $table->text("status")->nullable();
            $table->text("sizes")->nullable();
            $table->text("colors")->nullable();
            $table->text("content")->nullable();
            $table->text("price")->nullable();
            $table->text("old_price")->nullable();
            $table->text("url")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('import_amazon_previews');
    }
}
