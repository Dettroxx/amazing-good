<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateAuthorpost extends Migration
{
    public function up()
    {
        Schema::table('instagram_posts', function (Blueprint $table) {
            $table->integer("author")->unsigned()->index()->nullable(); 
            $table->foreign('author')->references('id')->on('users');
        }); 
    }

    public function down()
    {
        Schema::table('instagram_posts', function (Blueprint $table) {
            $table->dropColumn('author');  
        });
    }
}
