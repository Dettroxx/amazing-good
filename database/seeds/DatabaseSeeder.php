<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            UsersTableSeeder::class,
            PassportClientSeeder::class,
            LogStatusesSeeder::class,
            //InstagramPostsSeeder::class,
            //InstagramUsersSeeder::class,
            //InstagramCommentsSeeder::class,
            //InstagramTagsSeeder::class,
            //InstagramTagLinksSeeder::class,
            //InstagramDotsSeeder::class,
            //AmazonOffersSeeder::class,
            TagCategoriesSeeder::class,
            TagsSeeder::class,
            //LinkedCurrentTagsSeeder::class,
            //PostOfferLinksSeeder::class,
            //AmazonOfferCategoriesSeeder::class,
            //AmazonOfferCategoriesLinksSeeder::class,
            ImportInstagramPreviewSeeder::class,
            //ImportAmazonPreviewSeeder::class,
            InstagramConfigTagsSeeder::class
        ]);
    }
}
