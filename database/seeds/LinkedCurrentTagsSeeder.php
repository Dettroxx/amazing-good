<?php

use Illuminate\Database\Seeder;

class LinkedCurrentTagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/LinkedCurrentTags.sql');
        DB::unprepared($sql);
    }
}
