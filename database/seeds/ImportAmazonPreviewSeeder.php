<?php

use Illuminate\Database\Seeder;

class ImportAmazonPreviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/Import/ImportAmazonPreviews.sql');
        DB::unprepared($sql);
    }
}
