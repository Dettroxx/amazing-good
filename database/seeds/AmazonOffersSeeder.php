<?php

use Illuminate\Database\Seeder;

class AmazonOffersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/AmazonOffers.sql');
        DB::unprepared($sql);
    }
}
