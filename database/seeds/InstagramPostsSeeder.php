<?php

use Illuminate\Database\Seeder;

class InstagramPostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/InstagramPosts.sql');
        DB::unprepared($sql);
    }
}
