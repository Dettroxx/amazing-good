<?php

use Illuminate\Database\Seeder;
use App\LogStatus;

class LogStatusesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        LogStatus::insert([
            [
                "id" => 1,
                "message" => "Нажал кнопку сохранить"
            ],
			[
                "id" => 2,
                "message" => "Привязал оффер"
            ],
			[
                "id" => 3,
                "message" => "Отвязал оффер"
            ]
        ]);
    }
}
