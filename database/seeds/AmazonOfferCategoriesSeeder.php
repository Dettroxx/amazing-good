<?php

use Illuminate\Database\Seeder;

class AmazonOfferCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/AmazonOfferCategories.sql');
        DB::unprepared($sql);
    }
}
