<?php

use Illuminate\Database\Seeder;

class InstagramTagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/InstagramTags.sql');
        DB::unprepared($sql);
    }
}
