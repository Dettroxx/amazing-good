<?php

use Illuminate\Database\Seeder;

class InstagramCommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/InstagramComments.sql');
        DB::unprepared($sql);
    }
}
