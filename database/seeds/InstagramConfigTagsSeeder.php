<?php

use Illuminate\Database\Seeder;

class InstagramConfigTagsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/InstagramConfigTags.sql');
        DB::unprepared($sql);
    }
}
