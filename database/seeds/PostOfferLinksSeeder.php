<?php

use Illuminate\Database\Seeder;

class PostOfferLinksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/PostOfferLinks.sql');
        DB::unprepared($sql);
    }
}
