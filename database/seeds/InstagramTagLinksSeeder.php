<?php

use Illuminate\Database\Seeder;

class InstagramTagLinksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/InstagramTagLinks.sql');
        DB::unprepared($sql);
    }
}
