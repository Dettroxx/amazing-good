<?php

use Illuminate\Database\Seeder;

class TagCategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/TagCategories.sql');
        DB::unprepared($sql);
    }
}
