INSERT INTO `instagram_config_tags` (`id`, `type`, `name`, `strict`) VALUES
(1, 0, 'interiordesign', 1),
(2, 0, 'chair', 1),
(3, 0, 'table', 1),
(4, 0, 'sofa', 1),
(5, 1, 'chair', 1),
(6, 1, 'interiordesign', 1),
(7, 1, 'sofa', 1),
(8, 1, 'table', 1);