<?php

use Illuminate\Database\Seeder;

class ImportInstagramPreviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/Import/ImportInstagramPreview.sql');
        DB::unprepared($sql);
    }
}
