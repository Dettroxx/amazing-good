<?php

use Illuminate\Database\Seeder;

class PassportClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("oauth_clients")->insert([
            "id" => 1,
            "name" => "Password Client OAuth",
            "secret" => "NtwSNeylcxcAclz82dgSvw67DGUjQF6iYyJYKEZG",
            "redirect" => Config::get('app.url'),
            "personal_access_client" => 0,
            "password_client" => 1,
            "revoked" => 0
        ]);
    }
}
