<?php

use Illuminate\Database\Seeder;

class InstagramDotsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/InstagramDots.sql');
        DB::unprepared($sql);
    }
}
