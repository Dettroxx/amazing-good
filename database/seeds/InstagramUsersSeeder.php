<?php

use Illuminate\Database\Seeder;

class InstagramUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/InstagramUsers.sql');
        DB::unprepared($sql);
    }
}
