<?php

use Illuminate\Database\Seeder;

class AmazonOfferCategoriesLinksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $sql = file_get_contents(__DIR__ . '/sql/AmazonOfferCategoriesLink.sql');
        DB::unprepared($sql);
    }
}
