<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 10; $i++) {
            $id = $i+1;
            User::create([
                'name' => 'user'.$id,
                'email' => 'user'.$id.'@app.ru',
                'password' => Hash::make( 'passapp'.$id )
            ]);
        }
    }
}
